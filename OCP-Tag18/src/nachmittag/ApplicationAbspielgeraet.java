package nachmittag;


interface Tontraeger {}

class Schallplatte implements Tontraeger {}
class Tonband implements Tontraeger {}
class CD implements Tontraeger {}

class Abspielgeraet<T extends Tontraeger> {
	void abspielen(T t) { System.out.println("fertig");}
	
}


public class ApplicationAbspielgeraet {

	public static void main(String[] args) {
		
		Abspielgeraet<Schallplatte> a = new Abspielgeraet<Schallplatte>();
		Abspielgeraet<CD> a1 = new Abspielgeraet<CD>();
		Schallplatte s = new Schallplatte();
		Tonband tb = new Tonband();
		CD cd = new CD();
		
		
//		a.abspielen(cd);
		a.abspielen(s);
		a1.abspielen(cd);
	}
}
