package vormittag;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSplitPane;
import javax.swing.JTable;

import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class GUITest extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField user;

	static final String URL = "jdbc:mysql://localhost:3306/world?serverTimezone=Europe/Berlin";
	static String USER = "";
	static String PASSWORD = "";
	private JPasswordField password;

	public GUITest() {
		super("Datenbank-Login");
		setSize(400, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);

		JLabel lblDatenbanklogin = new JLabel("Datenbank-Login");
		panel.add(lblDatenbanklogin);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(new Rectangle(0, 0, 150, 0));
		getContentPane().add(panel_1, BorderLayout.WEST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{100};
		gbl_panel_1.rowHeights = new int[]{30, 30, 0};
		gbl_panel_1.columnWeights = new double[]{0.0};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);

		JLabel lblKennwort = new JLabel("Benutzer");
		GridBagConstraints gbc_lblKennwort = new GridBagConstraints();
		gbc_lblKennwort.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblKennwort.insets = new Insets(0, 0, 0, 5);
		gbc_lblKennwort.gridx = 0;
		gbc_lblKennwort.gridy = 0;
		panel_1.add(lblKennwort, gbc_lblKennwort);

		JLabel lblBenutzer = new JLabel("Passwort");
		GridBagConstraints gbc_lblBenutzer = new GridBagConstraints();
		gbc_lblBenutzer.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblBenutzer.gridx = 0;
		gbc_lblBenutzer.gridy = 1;
		panel_1.add(lblBenutzer, gbc_lblBenutzer);

		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{200, 0};
		gbl_panel_2.rowHeights = new int[]{30, 30, 0};
		gbl_panel_2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);

		user = new JTextField();
		GridBagConstraints gbc_user = new GridBagConstraints();
		gbc_user.fill = GridBagConstraints.BOTH;
		gbc_user.insets = new Insets(0, 0, 5, 0);
		gbc_user.gridx = 0;
		gbc_user.gridy = 0;
		panel_2.add(user, gbc_user);
		user.setColumns(10);

		password = new JPasswordField();
		GridBagConstraints gbc_password = new GridBagConstraints();
		gbc_password.insets = new Insets(0, 0, 5, 0);
		gbc_password.fill = GridBagConstraints.BOTH;
		gbc_password.gridx = 0;
		gbc_password.gridy = 1;
		panel_2.add(password, gbc_password);

		JPanel panel_3 = new JPanel();
		getContentPane().add(panel_3, BorderLayout.SOUTH);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[]{400};
		gbl_panel_3.rowHeights = new int[]{23, 0, 0, 0};
		gbl_panel_3.columnWeights = new double[]{1.0};
		gbl_panel_3.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);

		JTextPane textPane = new JTextPane();
		GridBagConstraints gbc_textPane = new GridBagConstraints();
		gbc_textPane.insets = new Insets(0, 0, 0, 5);
		gbc_textPane.fill = GridBagConstraints.BOTH;
		gbc_textPane.gridx = 0;
		gbc_textPane.gridy = 2;
		panel_3.add(textPane, gbc_textPane);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try (Connection conn = DriverManager.getConnection(URL,
						user.getText(), password.getText());
						Statement stmt = conn.createStatement(
								ResultSet.TYPE_SCROLL_INSENSITIVE,
								ResultSet.CONCUR_UPDATABLE)) {
					textPane.setText("Verbindung erfolgreich");
					String sql = "SELECT * FROM country";
					ResultSet rs = stmt.executeQuery(sql);
					createTableView(rs);

				} catch (SQLException er) {
					textPane.setText(
							"Verbindung nicht erfolgreich\n" + er.getErrorCode()
									+ " " + er.getLocalizedMessage());
					er.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.insets = new Insets(0, 0, 5, 0);
		gbc_btnLogin.gridx = 0;
		gbc_btnLogin.gridy = 0;
		panel_3.add(btnLogin, gbc_btnLogin);

	}

	public static void main(String[] args) {
		JFrame frame = new GUITest();
		frame.setVisible(true);

	}

	void createTableView(ResultSet rs) {
		JFrame dbFrame = new JFrame("Datenbank Ansicht");
		dbFrame.setSize(800, 600);
		dbFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		dbFrame.setLocationRelativeTo(null);
		dbFrame.getContentPane();

		int count = 0;

		try {

			while (rs.next()) {
				count++;
			}
			rs.first();
			ResultSetMetaData rsm = rs.getMetaData();
			Object[] header = new Object[rsm.getColumnCount()];
			Object[][] values = new Object[count][rsm.getColumnCount()];

			while (rs.next()) {
				for (int i = 0; i < header.length; i++) {
					header[i] = (rsm.getColumnName(i + 1));
				}
				break;
			}
			rs.beforeFirst();
			while (rs.next()) {
				for (int col = 1; col <= rsm.getColumnCount(); col++) {
					values[rs.getRow() - 1][col - 1] = (rs.getObject(col));
				}
			}

			DefaultTableModel tableModel = new DefaultTableModel(values,
					header);
			JTable table = new JTable(tableModel);
			TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(
					table.getModel());
			table.setRowSorter(sorter);
			// List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
			// sortKeys.add(new RowSorter.SortKey(4, SortOrder.ASCENDING));
			// sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
			// sorter.setSortKeys(sortKeys);
			JScrollPane pane = new JScrollPane(table);
			pane.setSize(800, 600);
			pane.setViewportView(table);
			dbFrame.add(pane);
			dbFrame.validate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbFrame.setVisible(true);
	}
}
