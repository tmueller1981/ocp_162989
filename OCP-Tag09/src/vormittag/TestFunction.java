package vormittag;

import java.util.function.Function;

public class TestFunction {

	static class Tier {
		int alter;
		
		public Tier(int alter) {
			this.alter = alter;
		}
		
		@Override
		public String toString() {
			return "Tier " + alter;
		}
	}
	
	static Function<Integer, Tier> f1 = x -> new Tier(x);
	public static void main(String[] args) {
		System.out.println(f1.apply(15));
	}
}
