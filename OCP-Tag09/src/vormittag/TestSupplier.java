package vormittag;

import java.util.Random;
import java.util.function.Supplier;

public class TestSupplier {
	
	public static void main(String[] args) {
		
		Supplier<String> i1 = new Supplier<String>() {
			Random r = new Random();
			int low = -20;
			int high = 20;
			int result = r.nextInt(high-low) + low;
			public String get() {
				return String.valueOf(result);
			}
		};
		System.out.println(i1.get());

	}
}
