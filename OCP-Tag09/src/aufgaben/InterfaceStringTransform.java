package aufgaben;

import java.util.ArrayList;


interface IStringTransform {
	ArrayList<String> transform(String[] arr);
	ArrayList<String> transform(String[] arr, int i);
}

public class InterfaceStringTransform implements IStringTransform {

	
	public static void main(String[] args) {
		String[] arr = {"mo","di","mi" };
		InterfaceStringTransform ift = new InterfaceStringTransform();
		ArrayList<String> list = ift.transform(arr);
		System.out.println("------Aufgabe 01 ------------");
		System.out.println(list);
		
		list.clear();
		list = ift.transform(arr,0);
		System.out.println("------Aufgabe 02 ------------");
		System.out.println(list);	
		
		list.clear();
		list = ift.transform(arr,2);
		System.out.println("------Aufgabe 03 ------------");
		System.out.println(list);		
		
		list.clear();
		list = ift.transform(arr,3);
		System.out.println("------Aufgabe 04 ------------");
		System.out.println(list);			
	}
	
	@Override
	public ArrayList<String> transform(String[] arr) {
		ArrayList<String> tmp = new ArrayList<>();
		for (String string : arr) {
			tmp.add(string.toUpperCase());
		}
		return tmp;
		
	}
	
	@Override
	public ArrayList<String> transform(String[] arr, int i) {
		ArrayList<String> tmp = new ArrayList<>();
		for (String string : arr) {
			switch(i) {
			case 0:
				tmp.add(string.toLowerCase());
				break;
			case 1:
				tmp.add(string.toUpperCase());
				break;
			case 2:
				tmp.add(string.toLowerCase() + ". ");
				break;		
			case 3:
				tmp.add(string.toLowerCase() + ". (" + string.length() + ")");
				break;					
			}				
		}
		return tmp;
		
	}	
}
