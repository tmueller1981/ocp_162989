package aufgaben;

import java.util.Random;

class Matrix {

	private int row;
	private int col;
	private int value;
	private int[][] matrix;
	
	public Matrix(int row, int col) {
		this.row = row;
		this.col = col;
		this.matrix = new int[row][col];
	}
	public Matrix(int row, int col, int value) {
		this(row, col);
		this.value = value;
		this.matrix = new int[row][col];
		for(int i = 0; i < this.matrix.length; i++) {
			for(int j = 0; j < this.matrix[i].length; j++) {
				this.matrix[i][j] = this.value;
			}
		}
	}
	public int get(int a, int b) {
		return matrix[a][b];
	}
	
	public static Matrix getRandomMatrix(int row, int col, int value) {
		Random r = new Random();
		Matrix m = new Matrix(row, col);
		for(int i = 0; i < m.matrix.length; i++) {
			for(int j = 0; j < m.matrix[i].length; j++) {
				m.matrix[i][j] = (int)r.nextInt(value);
			}
		}		
		
		return m;
		
	}
	
	@Override
	public String toString() {
		String res = "";
		for(int i = 0; i < this.matrix.length; i++) {
			for(int j = 0; j < this.matrix[i].length; j++) {
				res += String.valueOf(this.matrix[i][j]) + ", ";
			}
			res += "\n";
		}
		return res;
	}
}
public class IntMatrix {
	
	public static void main(String[] args) {
		
		Matrix m1 = new Matrix(2,3);
		Matrix m2 = new Matrix(5,3,100);
		Matrix m3 = Matrix.getRandomMatrix(4, 6, 200);
		
		System.out.println(m1);
		System.out.println("-------");
		System.out.println(m1.get(1, 2));
		System.out.println("-------");
		System.out.println(m3);
	}
}