package nachmittag;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoPeriod;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Properties;

public class HappyBirthday {

	public static void main(String[] args) {

		Properties props = System.getProperties();
		String name = props.getProperty("user.name");
		props.setProperty("user.birthday", "1981-05-30");
		String geb = props.getProperty("user.birthday");

		System.out.println("*** Aufgabe 01");
		System.out.println(
				"Hallo " + name + "\nDefault-Sprache: " + Locale.getDefault()
						.getLanguage());

		Locale def = Locale.getDefault();

		LocalDate date = LocalDate.now();

		LocalDate birth = LocalDate.parse(geb);
		LocalDate date2 = date.plusYears(1);
		ChronoPeriod time = ChronoPeriod.between(date, date2);

		DateTimeFormatter df = DateTimeFormatter.ofPattern("MMMM d, yyyy", def);
		System.out.println("\n*** Aufgabe 02");
		System.out.println("Hallo " + name + "\nHeute ist " + date.format(df));
		System.out.println("Es ist "
				+ date.format(DateTimeFormatter.ofPattern("EEEE", def)));
		System.out.println("Dein Geburtstag ist am: " + geb);
		System.out.println("Das sind noch " + time + "Tage");

		Locale.setDefault(new Locale("en"));
		def = Locale.getDefault();
		System.out.println("\n*** Aufgabe 03");
		System.out.println("Hallo " + name + "\nToday is " + date.format(df));
		System.out.println("It's "
				+ date.format(DateTimeFormatter.ofPattern("EEEE", def)));
		System.out.println("Your birthday is: " + geb);
	}
}
