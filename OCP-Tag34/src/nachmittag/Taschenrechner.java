package nachmittag;

import java.util.ResourceBundle;
import java.util.Scanner;

interface Operation {
	double execute(double n1, double n2);
}

public class Taschenrechner {

	public static void main(String[] args) {

		ResourceBundle resBundle = ResourceBundle
				.getBundle("res.taschenrechner");

		System.out.println("*** " + resBundle.getString("app.title") + " ***");

		final char repeatYesChar = resBundle.getString("repeat.yes.char")
				.charAt(0);

		char repeat = repeatYesChar;

		while (repeat == repeatYesChar) {
			System.out.print(resBundle.getString("prompt.first.number") + ": ");
			double a = readUserNumber();

			System.out.print(
					resBundle.getString("prompt.operator") + " (+, -, *, /): ");
			char operator = readUserChar();

			System.out
					.print(resBundle.getString("prompt.second.number") + ": ");
			double b = readUserNumber();

			Operation op = null;

			switch (operator) {
				case '+' :
					op = (n1, n2) -> n1 + n2;
					break;
				case '-' :
					op = (n1, n2) -> n1 - n2;
					break;
				case '*' :
					op = (n1, n2) -> n1 * n2;
					break;
				case '/' :
					op = (n1, n2) -> n1 / n2;
					break;

				default :
					System.out.println(
							"Fehler! Operator nicht unterstützt: " + operator);
					continue; // wieder von vorne nach einem Fehler
			}

			double result = op.execute(a, b);
			System.out.println(
					resBundle.getString("prompt.result") + ": " + result);

			System.out.print(resBundle.getString("prompt.repeat") + ":");
			repeat = readUserChar();
		}

	}

	@SuppressWarnings("resource")
	private static double readUserNumber() {
		return new Scanner(System.in).nextDouble();
	}

	@SuppressWarnings("resource")
	private static char readUserChar() {
		return new Scanner(System.in).nextLine()
				.charAt(0);
	}
}
