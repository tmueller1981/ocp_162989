package comparatorAndSearch;

interface Range {
	int getMin();
	int getMax();
	
	default int getRange() {
		return getMax() - getMin() + 1;
	}
}

interface BytePool {
	default int getRange() { return 256; }
}

class MyInts implements Range {
	public int getMin() { return -3; }
	public int getMax() { return 3; }
}

/**
 * 
 * wenn es Mehrdeutigkeit gibt bei den geerbten 'default'-Methoden,
 * muss in der Klasse die eigene Realisierung definiert werden! 
 *
 */
class MyBytes implements Range, BytePool {
	public int getMin() { return -10; }
	public int getMax() { return 10; }
	
	// muss sein, sonst merhdeutige Vererbung (aus Interface)
	public int getRange() {
//		return 21;
		return Range.super.getRange(); // ist ein spezieller Aufruf bei zweideutigen Namen in den Interfaces
	}
}

public class InterfaceDefaultMethods {

	public static void main(String[] args) {
		
		MyBytes var = new MyBytes();
		System.out.println( var.getRange());

	}
}
