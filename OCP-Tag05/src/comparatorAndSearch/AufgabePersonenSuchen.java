package comparatorAndSearch;

import java.util.Arrays;

class Person implements Comparable<Person>{
	String vorname, nachname;
	Person(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}
	@Override
	public int compareTo(Person o) {
		int erg = this.nachname.compareTo(o.nachname);
		if(erg == 0)
			erg = this.vorname.compareTo(o.vorname);
		return erg;
	}
	@Override
	public String toString() {
		return "Person: " + vorname + ", " + nachname;
	}
	
	
}

public class AufgabePersonenSuchen {

	public static void main(String[] args) {
		
		Person[] personen = {
				new Person("Albert", "Einstein"),
				new Person("Liselotte", "Pilcher"),
				new Person("Amadeus", "Mozart"),
				new Person("Rüdiger", "Hofmann"),
				new Person("Anna", "Hofmann")
				
		};
		Person key = new Person("Amadeus", "Mozart");
		
		int pos = Arrays.binarySearch(personen, key);
		System.out.printf("Gesucht nach %s. Pos.: %d %n", key, pos); // -1 weil unsortiert
		Arrays.sort(personen);
		System.out.printf("Gesucht nach %s. Pos.: %d %n", key, pos); // -1 weil unsortiert
	}
}
