package comparatorAndSearch;

/**
 * 
 * enum-Klasse erbt von Enum
 * Konstruktoren der enum-Klasse sind private (auch impliziet)
 *
 */

enum Size {
	SMALL, NORMAL, BIG; // statische Konstanten
}

public class Enums {

	public static void main(String[] args) {

		Size s1 = Size.SMALL;
		
		/*
		 * enums sind Comparable
		 */
		Comparable<Size> c1 = Size.BIG;
		
		int result = s1.compareTo(Size.NORMAL);
		System.out.println("result = " + result); // result = -1
		
		System.out.println("s1.ordinal: " + s1.ordinal()); // 0
		
		/**
		 * enum Klasse hat die statische Methode 'values()', die
		 * das Array mit den enum-Konstanten liefert
		 */
		
		//SMALL
		//NORMAL
		//BIG
		for(Size s : Size.values()) {
			System.out.println(s);
		}
		
	}

}
