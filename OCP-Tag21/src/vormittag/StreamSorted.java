package vormittag;

import java.util.stream.Stream;

public class StreamSorted {

	public static void main(String[] args) {

		Stream.of(1, 9, 15, 2, 3).sorted().forEach(System.out::println);
	}
}
