package vormittag;

import java.util.Arrays;
import java.util.Comparator;

class Person {
	private String name;
	private int gewicht;

	public Person(String name, int gewicht) {
		this.name = name;
		this.gewicht = gewicht;
	}

	public String getName() {
		return name;
	}

	public int getGewicht() {
		return gewicht;
	}

	@Override
	public String toString() {
		return name + " (" + gewicht + ")";
	}
}

public class AufgabeOperationsSimple {

	public static void main(String[] args) {

		Person[] personen = { new Person("Tom", 70), new Person("Jerry", 50), new Person("Peter", 60),
				new Person("Mary", 45), };

		a1(personen);
		a2(personen);

	}

	/*
	 * Bitte mit einer Pipline die Anzahl der Personen ermitteln, mit dem Gewicht
	 * zwischen 50 bis 65 Kilo.
	 */
	static void a1(Person[] person) {

		long c = Arrays.stream(person).filter(p -> p.getGewicht() >= 50 && p.getGewicht() <= 65).count();
		System.out.println(c);
	}

	static void a2(Person[] person) {
		Arrays.stream(person).max(Comparator.comparing(Person::getName)).ifPresent(System.out::println);
	}

}
