package vormittag;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class StreamBilden {

	static Integer nextInt() {
		return new Random().nextInt();
	}
	
	public static void main(String[] args) {
		
		a1();
		a2();
		a3();
		a4();
	}
	
	static void a1() {
		System.out.println("*** A1");
		
		List<Integer> list1 = Arrays.asList( 1, 2, 3);
		List<Integer> list2 = Arrays.asList(55,77);
		
		// A
		List<List<Integer>> list3 = Arrays.asList(list1, list2);
		for(List<Integer> e : list3) {
			System.out.println("size = " + e.size() + ". elements = " + e);
		}
		
		// B
		System.out.println("*** mit Stream");
		
		Stream.of(list1, list2).forEach(e -> System.out.println("size = " + e.size() + ". elements = " + e));
	}
	
	static void a2() {
		System.out.println("\n*** A2");
		for(int i = 1; i < 5; i++) {
			System.out.println( nextInt() );
		}
		
		System.out.println("*** mit Stream");
		
		Supplier<Integer> supplier = new Supplier<Integer>() {
			@Override
			public Integer get() {
				return StreamBilden.nextInt();
			}
		};
		Stream.generate(StreamBilden::nextInt).limit(4).forEach(System.out::println);
		
		Stream.generate(() -> StreamBilden.nextInt()).limit(4).forEach(System.out::println);
	}
	
	static void a3() {
		System.out.println("*** A3");
		
		for (int i = 10; i >= 1; i--) {
			System.out.println(i);
		}
		
		System.out.println("*** mit Stream");
		
		Integer seed = 10;
		UnaryOperator<Integer> op = x -> --x;
		Stream.iterate(seed, op).limit(10).forEach(System.out::println);
	}
	
	static void a4() {
		System.out.println("*** A4");
		
		String[] a1 = { "a", "b" };
		String[] a2 = { "c", "d"};
		
		// A
		String[][] a3 = { a1, a2 };
		for(String[] arr : a3) {
			for(String s : arr) {
				System.out.println(s);
			}
		}
		
		// B
		System.out.println("*** mit Stream");
		Stream.concat(Stream.of(a1), Stream.of(a2)).forEach(System.out::println);;
	}
}
