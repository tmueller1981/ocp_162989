package vormittag;

import java.util.Arrays;

public class StreamDistinct {

	public static void main(String[] args) {
		a1();
	}

	static void a1() {

		class Auto {
			int baujahr;

			public Auto(int baujahr) {
				this.baujahr = baujahr;
			}

			@Override
			public String toString() {
				return "Auto von " + baujahr;
			}

			@Override
			public boolean equals(Object a) {
				return baujahr == ((Auto) a).baujahr;
			}

			@Override
			public int hashCode() {
				return baujahr;
			}
		}

		Auto[] array = { new Auto(2000), new Auto(2011), new Auto(2000), new Auto(2017) };

		Arrays.stream(array).distinct().forEach(System.out::println);

	}
}
