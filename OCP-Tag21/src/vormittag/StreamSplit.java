package vormittag;

import java.util.Arrays;
import java.util.List;

public class StreamSplit {

	public static void main(String[] args) {
		a1();
	}

	static void a1() {

		List<String> list = Arrays.asList("Java ist toll", "Astrophysik ist besser", "Streams sind einfach");

		list.stream().map(s -> s.split(" ")).flatMap(Arrays::stream).forEach(System.out::println);

	}
}
