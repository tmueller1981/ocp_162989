package vormittag;

import java.util.stream.Stream;

public class StreamUmwandeln {

	public static void main(String[] args) {
		a1();
	}

	static void a1() {

		class Tier {
			int alter;

			public Tier(int alter) {
				this.alter = alter;
			}

			@Override
			public String toString() {
				return "Tier (" + alter + ")";
			}
		}

		Stream.of(2, 4, 3, 7).map(x -> new Tier(x))
				// .map(Tier::new); alternative
				.forEach(System.out::println);
		;
	}
}
