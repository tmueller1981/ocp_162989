package nachmittag;

import java.util.Comparator;
import java.util.Locale;
import java.util.stream.Stream;

public class StreamMinMax {

	public static void main(String[] args) {

		Locale[] locales = Locale.getAvailableLocales();
		Stream.of(locales).sorted(Comparator.comparing(Locale::getDisplayLanguage)).forEach(StreamMinMax::print);

		Comparator<Locale> cmp = Comparator.comparing(Locale::getDisplayLanguage);
//		Arrays.stream(locales).max(cmp).ifPresent(loc -> print(loc));
	}

	static void print(Locale loc) {
		System.out.printf("%-3s | %22s | %3s | %s %n", loc.getLanguage(), loc.getDisplayLanguage(), loc.getCountry(),
				loc.getDisplayCountry());
	}
}
