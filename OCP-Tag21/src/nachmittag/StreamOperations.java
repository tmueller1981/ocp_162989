package nachmittag;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

class Tier {
	private String name;

	public Tier(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Tier " + name;
	}

	@Override
	public boolean equals(Object obj) {
		Tier t2 = (Tier) obj;
		return this.name.equals(t2.name);
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
}

public class StreamOperations {

	public static void main(String[] args) {

		System.out.println("*** Aufgabe A");

		List<Integer> list = Arrays.asList(13, 15, 17, 19, 21);
		list.stream().filter(x -> x == 15 || x == 19).forEach(x -> System.out.println("Treffer: " + x));

		System.out.println("\n*** Aufgabe B");
		Integer[] array = { 1, 4, 7, 3, -8 };

		Arrays.stream(array).map(x -> x % 2 == 0 ? "gerade" : "ungerade").forEach(System.out::println);

		System.out.println("\n*** Aufgabe C");

		List<String> list1 = Arrays.asList("Tom", "Jerry", "Rex");
		list1.stream().map(s -> new Tier(s)).forEach(System.out::println);

		System.out.println("\n*** Aufgabe D");
		Stream.generate(() -> new Random().nextInt(20 + 20) - 20).limit(30).filter(x -> x < -15 || x > -10)
				.map(x -> x.doubleValue()).forEach(System.out::println);

		System.out.println("\n*** Aufgabe E");
		Tier[] array1 = { new Tier("Rex"), new Tier("Tom"), new Tier("Jerry"), new Tier("Tom"), new Tier("Jerry"), };
		Arrays.stream(array1).distinct().forEach(System.out::println);

		System.out.println("\n*** Aufgabe F");
		List<String> mailsErsthelfer = Arrays.asList("tom@mycompany.com", "jerry@mycompany.com");
		List<String> mailsIT = Arrays.asList("tom@mycompany.com", "mary@mycompany.com");
		List<String> mailsQM = Arrays.asList("peter@mycompany.com", "jerry@mycompany.com");
		Stream.of(mailsErsthelfer, mailsIT, mailsQM).flatMap(list3 -> list3.stream()).map(s -> s.split("@")[0])
				.distinct().forEach(s -> System.out.println(s));
	}
}
