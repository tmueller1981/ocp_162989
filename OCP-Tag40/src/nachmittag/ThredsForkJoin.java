package nachmittag;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ThredsForkJoin {

	public static void main(String[] args) throws IOException {

		System.out.println("*** Aufgabe 01");
		List<String> words = new ArrayList<>();
		words.addAll(readWords());
		System.out.println(words);

		System.out.println("\n*** Aufgabe 02");
		// a2(words);

		System.out.println("\n*** Aufgabe 03");
		a3(words);

		System.out.println("\n*** Aufgabe 04");
		// int[] zahlen = a4();
		//
		// for (int a : zahlen)
		// System.out.println(a);
	}

	static int[] a4() {

		class MyAction extends RecursiveAction {
			int[] list;

			public MyAction(int[] arr) {
				this.list = arr;
			}

			@Override
			protected void compute() {

				for (int i = 0; i < list.length; i++) {
					if (list[i] < 0)
						list[i] = 0;
				}

			}
		}
		int[] arr = new int[100];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = new Random().nextInt(50 + 50) - 50;

		}

		ForkJoinPool pool = new ForkJoinPool();
		RecursiveAction action = new MyAction(arr);
		pool.invoke(action);
		return arr;
	}

	static void a3(List<String> list) {

		ForkJoinPool pool = new ForkJoinPool();
		RecursiveTask<String> action = new RecursiveTask<String>() {

			@Override
			protected String compute() {
				Optional<String> o = list.stream()
						// .reduce((a, b) -> a.compareTo(b))
						.max(Comparator.naturalOrder());

				return o.get();

			}
		};

		String max = pool.invoke(action);
		System.out.println(max);
	}

	static void a2(List<String> list) {

		ForkJoinPool pool = new ForkJoinPool();
		RecursiveAction action = new RecursiveAction() {

			@Override
			protected void compute() {

				list.stream()
						.map((s) -> s.toUpperCase())
						.forEach(System.out::println);
			}
		};

		pool.invoke(action);

	}

	static Collection<String> readWords() throws IOException {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(
					"E:\\01_Dokumente\\Java Entwicklung\\OCP_162989\\WS_TN17\\ocp-online\\OCP-Tag40\\src\\nachmittag\\corncob_lowercase.txt")));
			List<String> words = new ArrayList<>();
			while (br.readLine() != null) {
				words.add(br.readLine());
			}
			return words;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		return null;

	}
}
