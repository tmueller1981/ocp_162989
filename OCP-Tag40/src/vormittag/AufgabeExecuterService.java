package vormittag;

import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Supplier;

public class AufgabeExecuterService {

	public static void main(String[] args)
			throws InterruptedException, ExecutionException {

		// a1();
		// a2();
		a3();
	}

	static void a3() throws InterruptedException {

		Random random = new Random();
		Supplier<Callable<Integer>> taskSupplier = () -> {
			return random::nextInt;
		};

		List<Callable<Integer>> tasks = Stream.generate(taskSupplier)
				.limit(100)
				.collect(Collectors.toList());

		ExecutorService service = Executors.newFixedThreadPool(5);

		List<Future<Integer>> futures = service.invokeAll(tasks);

		Function<Future<Integer>, Integer> mapper = future -> {
			try {
				return future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
			return null;
		};
		Integer summe = futures.stream()
				.map(mapper)
				.reduce(0, (a, b) -> a + b);
		System.out.println("summe: " + summe);
		service.shutdown();
	}

	static void a2() throws InterruptedException, ExecutionException {

		Random random = new Random();
		Callable<Double> task = () -> {
			return DoubleStream.generate(random::nextDouble)
					.limit(10)
					.sum();

		};

		ExecutorService service = Executors.newCachedThreadPool();
		Future<Double> future = service.submit(task);
		System.out.println("get: " + future.get());
		service.shutdown();
	}

	static void a1() throws InterruptedException, ExecutionException {

		Random random = new Random();

		Runnable task1 = () -> {
			Stream.generate(random::nextDouble)
					.limit(10)
					.forEach(System.out::println);
		};

		ExecutorService service = Executors.newSingleThreadExecutor();
		Future<?> future1 = service.submit(task1);
		System.out.println("get: " + future1.get());
		service.shutdown();

	}
}
