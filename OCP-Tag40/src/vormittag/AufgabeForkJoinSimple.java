package vormittag;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.*;

public class AufgabeForkJoinSimple {

	@SuppressWarnings("serial")
	static class MyAction extends RecursiveAction {
		List<Integer> list;

		public MyAction(List<Integer> list) {
			this.list = list;
		}

		@Override
		protected void compute() {
			if (list.size() < 3) {
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i) < 0)
						list.set(i, 0);

				}

				System.out.println("Kleine Liste: " + list);
			} else {
				int mitte = list.size() / 2;

				List<Integer> listLinks = list.subList(0, mitte);
				List<Integer> listRechts = list.subList(mitte, list.size());

				MyAction taskLinks = new MyAction(listLinks);
				MyAction taskRechts = new MyAction(listRechts);

				// und an den ForkJoin zum Ausf�hren �bergeben:
				invokeAll(taskLinks, taskRechts);
			}

		}
	}

	public static void main(String[] args) {

		List<Integer> list = Arrays.asList(1, -7, 22, 0, -5, 8);

		ForkJoinPool pool = new ForkJoinPool();
		RecursiveAction action = new MyAction(list);
		pool.invoke(action);

	}
}
