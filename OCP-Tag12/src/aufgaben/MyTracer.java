package aufgaben;

import java.util.function.Supplier;

public class MyTracer {
	public enum Level {
		NONE, TRACE
	}
	
	private final Level level;
	
	public MyTracer(Level level) {
		this.level = level;
	}

	//	public void trace(String message) {
	//	if (level == Level.TRACE) {
	//		System.out.println(message);
	//	}
	//}
	public void trace(Supplier<String> msgSupplier) {
		if (level == Level.TRACE) {
			String message = msgSupplier.get();
			System.out.println(message);
		}
	}
}
class Foo {
	MyTracer tracer = new MyTracer(MyTracer.Level.TRACE);
	
	public void m(String[] args) {		
		tracer.trace( () -> "args length: " + args.length);
		//...
	}
}