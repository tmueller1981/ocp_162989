package aufgaben;

import java.util.function.BiFunction;
import java.util.function.Function;

class StringTransform  {
	
	public String process(String s) {
		String newS = s;
		newS = addTransformation(st -> st.toUpperCase(), s);
		newS = addTransformation( st -> st + "!" , newS);
		return newS;
	}
	
	public String addTransformation(Function<String, String> f, String s) {
		return f.apply(s);
	}
}

public class StringTrans {

	public static void main(String[] args) {
        
        // Transformationen vordefinieren:
        StringTransform t1 = new StringTransform();
        
        // Transformationen durchführen:
        String s = t1.process("Hallo");
        System.out.println(s); // HALLO!
        
        s = t1.process("Java ist toll");
        System.out.println(s); // JAVA IST TOLL!
    }
}
