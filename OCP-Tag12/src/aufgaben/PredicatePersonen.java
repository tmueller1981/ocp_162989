package aufgaben;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

class FilterJahr implements Predicate<Person> {

	int minGeb;
	
	public FilterJahr(int minGeb) {
		this.minGeb = minGeb;
		
	}
	
	@Override
	public boolean test(Person t) {
		return this.minGeb < t.geburt;
	}


	
}

class Person {
	String vorname, nachname;
	public int geburt;
	
	public Person(String vorname, String nachname, int geburt) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburt = geburt;
	}
	
	@Override
	public String toString() {
		return vorname + " " + nachname + " (" + geburt + ")";
	}	
}


public class PredicatePersonen {

	public static void main(String[] args) {
		
		Person[] per = { new Person("Torben", "Müller", 1981),
				new Person("Cornelia", "Wendt",1974),
				new Person("Casey", "Westman", 2003),
				new Person("Daniela", "Müller", 1960),
				new Person("Rudi", "Kal", 1962)
		};
		
		System.out.println("*** A3. Personen, die nach 1960 geboren wurden: ");
		
		Predicate<Person> p1 = new FilterJahr(1970);
		List<Person> list = filtern(per, p1);
		print(list);
		
		System.out.println("*** A4. Personen, die im Nachnamen ein a haben: ");
		Predicate<Person> p2 = new FilterNachname();
		list.clear();
		list = filtern(per, p2);
		print(list);
		
		/**
		 * Lokale Klasse ist innerhalb der Main
		 */
		class NachnameMitLaenge implements Predicate<Person> {

			@Override
			public boolean test(Person t) {
				return t.nachname.length() > 4;
			}
			
		}
		
		System.out.println("*** A5. Personen, die im Nachnamen mindestens 5 Zeichen haben: ");
		Predicate<Person> p3 = new NachnameMitLaenge();
		list.clear();
		list = filtern(per, p3);
		print(list);
		
		System.out.println("*** A7. Personen, die in einem Schaltjahr geboren wurden:");
		Predicate<Person> p4 = (Person pe) -> { int geburtsjahr = pe.geburt; return Year.isLeap(geburtsjahr); };
		list.clear();
		list = filtern(per, p4);
		print(list);
	}
	
	static class FilterNachname implements Predicate<Person> {
		
		@Override
		public boolean test(Person t) {
			return t.nachname.contains("a");
		}
	}
	
	
	static List<Person> filtern(Person[] per, Predicate<Person> p) {
		List<Person> tmp = new ArrayList<>();
		for(Person man : per) {
			if(p.test(man)) {
				tmp.add(man);
			}
		}
		
		return tmp;
	}
	
	static void print(List<Person> personen) {
		for (int i = 0; i < personen.size(); i++) {
			System.out.printf("%d. %s %n", i+1, personen.get(i));
		}
	}
	
}
