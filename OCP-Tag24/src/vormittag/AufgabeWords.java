package vormittag;

import java.util.Comparator;

import words.Words;

public class AufgabeWords {

	public static void main(String[] args) {

		a1();
		a2();
	}

	static void a1() {
		System.out.println("*** A1");

		System.out.println("Das l�ngste Wort: ");
		Words.englishWords()
				.stream()
				.max(Comparator.comparing(String::length))
				.ifPresent(System.out::println);
	}

	static void a2() {
		System.out.println("\n*** A2");
		System.out.println("W�rter mit der L�nge 5: " + Words.englishWords()
				.stream()
				.filter(s -> s.length() == 5)
				.count());
	}
}
