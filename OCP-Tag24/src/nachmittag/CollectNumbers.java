package nachmittag;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CollectNumbers {

	static String[] arr = {"1,2,3,4,5", "7,6,5,4,3", "123,456",};

	public static void main(String[] args) {

		List<Integer> list;

		// A
		list = new ArrayList<>();
		for (String s : arr) {
			String[] stringNumbers = s.split(",");

			for (String sNum : stringNumbers) {
				Integer num = Integer.valueOf(sNum);
				list.add(num);
			}
		}
		// B

		System.out.println(list); // [1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 123, 456]

		a1();
		a2();
	}

	static void a1() {

		System.out.println("\nAufgabe 01");

		List<Integer> list = Arrays.stream(arr)
				.map(s -> s.split(","))
				.flatMap(subArr -> Arrays.stream(subArr))
				.map(strNumber -> Integer.valueOf(strNumber))
				.collect(() -> new ArrayList<>(),
						(lst, value) -> lst.add(value),
						(lst1, lst2) -> lst1.addAll(lst2));

		System.out.println(list);
	}

	static void a2() {
		System.out.println("\nAufgabe 02");

		List<Integer> list = Arrays.stream(arr)
				.map(s -> s.split(","))
				.flatMap(subArr -> Arrays.stream(subArr))
				.map(strNumber -> Integer.valueOf(strNumber))
				.filter(i -> i % 2 == 0)
				.collect(() -> new ArrayList<>(),
						(lst, value) -> lst.add(value),
						(lst1, lst2) -> lst1.addAll(lst2));

		System.out.println(list);
	}
}
