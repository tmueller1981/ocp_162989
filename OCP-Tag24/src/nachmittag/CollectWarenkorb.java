package nachmittag;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

class Produkt {
	private String name;
	private int preis;

	public Integer getPreis() {
		return this.preis;
	}

	public Produkt(String name, int preis) {
		this.name = name;
		this.preis = preis;
	}
	// Konstruktoren und Methoden hier, wenn n�tig...
}

class Bestellung {
	private String produktName;
	private int anzahl; // gew�nschte Anzahl der Produkt-Objekte

	public Bestellung(String produktname, int anzahl) {
		this.produktName = produktname;
		this.anzahl = anzahl;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public String getProduktName() {
		return produktName;
	}
}

public class CollectWarenkorb {

	private static Map<String, Integer> mapProduktpreise = new HashMap<>();

	public static void main(String[] args) {

		List<Produkt> warenkorb = new ArrayList<>();
		warenkorb.add(new Produkt("Brot", 129));
		warenkorb.add(new Produkt("Wurst", 230));
		warenkorb.add(new Produkt("Milch", 99));
		warenkorb.add(new Produkt("Milch", 99));

		List<Bestellung> bestellungen = new ArrayList<>();
		bestellungen.add(new Bestellung("Brot", 3));
		bestellungen.add(new Bestellung("Wurst", 1));
		bestellungen.add(new Bestellung("Milch", 2));

		a1(warenkorb);
		a2(bestellungen);
	}

	static void a1(List<Produkt> p) {
		System.out.println("*** Aufgabe 01");

		Integer sum = p.stream()
				.map(Produkt::getPreis)
				.reduce((a, b) -> a + b)
				.get();

		System.out.println(sum);

	}

	static void a2(List<Bestellung> p) {
		System.out.println("*** Aufgabe 02");

		List<Produkt> warenkorb = buildWarenkorb(p);
		System.out.println(warenkorb);

	}

	static List<Produkt> buildWarenkorb(List<Bestellung> bestellungen) {

		Function<Bestellung, List<Produkt>> bestellungZuProdukten = (
				Bestellung bst) -> {
			List<Produkt> produkte = new ArrayList<>();

			for (int i = 0; i < bst.getAnzahl(); i++) {
				String produktName = bst.getProduktName();
				Integer preis = mapProduktpreise.get(produktName);
				Produkt p = new Produkt(produktName, preis);
				produkte.add(p);
			}

			return produkte;
		};

		return bestellungen.stream() // Stream<Bestellung>
				.map(bestellungZuProdukten) // Stream<List<Produkt>>
				.flatMap(List::stream) // Stream<Produkt>
				.collect(Collectors.toList());
	}
}
