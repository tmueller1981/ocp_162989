package vormittag;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.*;

public class ExecuterServiceSimple {

	static int count;

	public static void main(String[] args) throws InterruptedException {

		v2();
	}

	static void v2() throws InterruptedException {

		ExecutorService service = Executors.newFixedThreadPool(4);

		Collection<Callable<Integer>> tasks = new ArrayList<>();
		for (int i = 0; i < 200; i++) {
			Callable<Integer> t = () -> {
				for (int j = 0; j < 10000; j++) {
					synchronized (ExecutorService.class) {
						count++;
					}

				}

				return 0;
			};

			tasks.add(t);
		}

		service.invokeAll(tasks);
		System.out.println(count);
		service.shutdown();
	}

	static void v1() {
		// ExecutorService service = Executors.newSingleThreadExecutor();
		ExecutorService service = Executors.newFixedThreadPool(4);

		Runnable task = () -> {
			for (int i = 0; i < 10_000; i++) {
				synchronized (ExecuterServiceSimple.class) {
					count++;
				}

			}
		};
		for (int i = 0; i < 200; i++) {
			service.execute(task);
		}
		service.shutdown();
		while (!service.isTerminated()) {
			;
		}

		System.out.println(count);
	}
}
