package vormittag;
import java.util.*;

public class AufgabeSetAbfahrtzeiten {

	public static void main(String[] args) {
		
		TreeSet<String> abfahrtzeiten = createAbfahrtzeiten();
		
		System.out.println(abfahrtzeiten);
		System.out.println("***");
		System.out.println("erste Abfahrtzeit nach 12:03: " + abfahrtzeiten.higher("12:03"));
		System.out.println("erste Abfahrtzeit vor 12:03: " + abfahrtzeiten.lower("12:03"));
		
		System.out.println("erste Abfahrtzeit nach 17:12 inklusive: " + abfahrtzeiten.ceiling("17:12"));
		System.out.println("erste Abfahrtzeit nach 17:12 exklusive: " + abfahrtzeiten.higher("17:12"));
		
		System.out.println("Abfahrtzeiten zwischen 12:00 und 13:00: " + abfahrtzeiten.subSet("12:00", "13:00"));
		System.out.println("Abfahrtzeiten zwischen 11:52 exklusive und 13:12 inlusive; " + abfahrtzeiten.subSet("11:52", false, "13:12", true));
		
		System.out.println("erstmögliche Abfahrtzeit: " + abfahrtzeiten.first());
		System.out.println("letzte Abfahrtzeit: " + abfahrtzeiten.last());
	}
	
	static TreeSet<String> createAbfahrtzeiten() {
		
		TreeSet<String> abfahrtzeiten = new TreeSet<>();
		
		for(int stunde = 6; stunde < 24; stunde++) {
			
			for (int minute = 12; minute < 60; minute += 20) {
				String zeit = String.format("%02d:%02d", stunde, minute);
				abfahrtzeiten.add(zeit);
			}
			
		}
		
		return abfahrtzeiten;
	}
}
