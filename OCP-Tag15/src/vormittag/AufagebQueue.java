package vormittag;
import java.util.*;


public class AufagebQueue {

	static class Dozent {
		String name;
		
		Dozent(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			
			return name;
		}
		
	}
	
	public static void main(String[] args) {
		
		Comparator<Dozent> cmp = (d1, d2) -> d1.name.compareTo(d2.name);
		PriorityQueue<Dozent> snake = new PriorityQueue<>( cmp );
		snake.add(new Dozent("Dozent 1"));
		snake.add(new Dozent("Dozent 2"));
		snake.add(new Dozent("Dozent 3"));
		snake.add(new Dozent("Dozent 4"));
		snake.add(new Dozent("Dozent 5"));
		
		
		System.out.println("Alle Ausgeben:");
		System.out.println(snake);
		
		System.out.println("Aufgabe 2");
		while(!snake.isEmpty()) {
			System.out.println("Entfernt: " + snake.remove());
		}
		System.out.println(snake.toString());
	}
	
	
	
}
