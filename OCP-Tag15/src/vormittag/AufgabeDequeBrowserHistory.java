package vormittag;

import java.util.ArrayDeque;
import java.util.Deque;

class BrowserHistory {
	private Deque<String> urls = new ArrayDeque<>();
	Deque<String> tmp = new ArrayDeque<>();
	private static final int MAX_URLS = 5;
	
	public void open(String url) {
		if(urls.size() == MAX_URLS) {
			urls.removeFirst();
		}
		urls.addLast(url);
	}
	
	public String getCurrent() {
		return urls.getLast();
	}
	
	public void openPrevious() {
		if(urls.size() == 1)
			throw new IllegalStateException("previous history is empty");
		tmp.addLast(urls.removeLast());
	}
	
	public void openNext() {
		if(urls.size() == MAX_URLS)
			throw new IllegalStateException("next history is empty");
		urls.addLast(tmp.removeLast());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int cnt = 0;
		for (String url : urls) {
			if(++cnt == urls.size())
				break;
			sb.append(url).append(" < ");
		}
		if(!urls.isEmpty())
			sb.append("[").append(urls.getLast()).append("]");
			
		return sb.toString();
	}
}

public class AufgabeDequeBrowserHistory {

	public static void main(String[] args) {
		
		BrowserHistory h = new BrowserHistory();
		
		h.open("u1.com");
		h.open("u2.com");
		h.open("u3.com");
		h.open("u4.com");
		h.open("u5.com");
		System.out.println("*** Teil Aufgabe 2");
		System.out.println(h);
		
		h.open("u6.com");
		System.out.println("\n*** Teil Aufgabe 3");
		System.out.println(h);
		String url = h.getCurrent();
		System.out.println("Aktuelle Seite: " + url);
		
		h.openPrevious();
		h.openPrevious();
		h.openPrevious();
		h.openPrevious();
		System.out.println("\n*** Teil Aufgabe 4");
		System.out.println("Aktuelle Seite: " + h.getCurrent());
		System.out.println(h);

		System.out.println("*** Teil Aufgabe 5");
		try {
			h.openPrevious();
		} catch (IllegalStateException e) {
			System.out.println( e.getMessage() ); // previous history is empty
		}
		System.out.println( "Aktuelle Seite: " + h.getCurrent() );
		
		System.out.println("\n*** Teil Aufgabe 6");
		h.openNext();
		h.openNext();
		h.openNext();
		h.openNext();
		System.out.println("Aktuelle Seite: " + h.getCurrent());
		System.out.println(h);
		
		System.out.println("\n*** Teil Aufgabe 7");
		try {
			h.openNext();
		} catch (IllegalStateException e) {
			System.out.println( e.getMessage() ); // next history is empty
		}
		System.out.println( "Aktuelle Seite: " + h.getCurrent() );
		
		System.out.println("\n*** Teil Aufgabe 8");
		
		h.openPrevious();
		h.openPrevious();
		System.out.println(h + " > " + h.tmp.removeLast() + " > " + h.tmp.removeLast());
		
		h.open("url7.com");
		System.out.println("\n*** Teil Aufgabe 9");
		System.out.println(h);
	}
}
