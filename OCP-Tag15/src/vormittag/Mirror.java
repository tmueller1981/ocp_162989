package vormittag;

import java.util.ArrayDeque;
import java.util.Deque;

public class Mirror {
	
	ArrayDeque<Object> deque = new ArrayDeque<>();
	
	public static void main(String[] args) {
		Mirror m = new Mirror();
		
		for (char ch = 'a'; ch < 'g'; ch++) {
			m.add(ch);
			System.out.println(m);
		}
		
		while( !m.isEmpty() ) {
			System.out.println(m);
			m.remove();
		}
	}
	
	void add(char c) {
		deque.add(c);
//		deque.push(c);
	}
	void remove() {
		deque.remove();
//		deque.removeLast();
	}
	boolean isEmpty() {
		return deque.isEmpty();
	}
	
	@Override
	public String toString() {
		Deque<Object> mirror = new ArrayDeque<>();
		mirror.add('|');
		
		for(Object character : deque) {
			mirror.addFirst(character);
			mirror.addLast(character);
		}
		StringBuilder sb = new StringBuilder();
		for(Object character : mirror) {
			sb.append(character);
		}
		return sb.toString();
	}
}
