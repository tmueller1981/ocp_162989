package vormittag.test;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import vormittag.logic.Zinsrechner;

public class ZinsrechnerTest {

	public static void main(String[] args) {

		int startkapital = 1000;
		double zinssatz = 5;
		double laufzeit = 5;

		double endkapital = Zinsrechner.berechneEndkapaital(startkapital,
				zinssatz, laufzeit);

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		System.out.printf("Endkapital: %,.2f �", endkapital);
		// System.out.println(nf.format(endkapital));

		nf = nf.getInstance();
		laufzeit = Zinsrechner.berechneLaufzeit(1000, 5, 2000);
		// System.out.println("\nLaufzeit: " + nf.format(laufzeit));
		System.out.printf("%n %.1f", laufzeit);
	}
}
