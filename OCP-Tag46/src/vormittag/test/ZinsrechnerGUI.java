package vormittag.test;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;

import vormittag.logic.Zinsrechner;

import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import javax.swing.JTable;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.Component;

public class ZinsrechnerGUI {

	private JFrame frame;
	static private JTextField startkapital;
	private JTextField zinssatz;
	private JTextField laufzeit;
	private JTextField endkapital;
	private JTable table = new JTable();
	private DefaultTableModel dataModel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(
							UIManager.getSystemLookAndFeelClassName());
					ZinsrechnerGUI window = new ZinsrechnerGUI();
					window.frame.setVisible(true);
					startkapital.requestFocusInWindow();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ZinsrechnerGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane()
				.setBackground(Color.ORANGE);
		frame.setSize(new Dimension(450, 500));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane()
				.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 434, 30);
		panel.setBackground(Color.ORANGE);
		frame.getContentPane()
				.add(panel);
		panel.setLayout(null);

		JLabel lblZinsrechner = new JLabel("Zinsrechner");
		lblZinsrechner.setBounds(169, 5, 95, 20);
		lblZinsrechner.setFont(new Font("Tahoma", Font.BOLD, 16));
		panel.add(lblZinsrechner);

		JRadioButton rdbtnEndkapital = new JRadioButton("Endkapital");
		rdbtnEndkapital.setBackground(Color.ORANGE);
		rdbtnEndkapital.setMnemonic(KeyEvent.VK_K);
		rdbtnEndkapital.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				endkapital.setEditable(false);
			}
		});

		JPanel flowHolder = new JPanel();
		flowHolder.setBounds(0, 30, 200, 200);
		frame.getContentPane()
				.add(flowHolder);
		flowHolder.setBackground(Color.ORANGE);
		FlowLayout flowLayout = (FlowLayout) flowHolder.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);

		JPanel panelLeft = new JPanel();
		panelLeft.setBounds(new Rectangle(50, 0, 0, 0));
		flowHolder.add(panelLeft);
		panelLeft.setBackground(Color.ORANGE);
		GridBagLayout gbl_panelLeft = new GridBagLayout();
		gbl_panelLeft.columnWidths = new int[]{150};
		gbl_panelLeft.rowHeights = new int[]{30, 30, 30, 30, 30};
		gbl_panelLeft.columnWeights = new double[]{0.0};
		gbl_panelLeft.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		panelLeft.setLayout(gbl_panelLeft);

		JLabel lblStartkapital = new JLabel("Startkapital");
		lblStartkapital.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblStartkapital = new GridBagConstraints();
		gbc_lblStartkapital.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblStartkapital.insets = new Insets(0, 0, 5, 0);
		gbc_lblStartkapital.gridx = 0;
		gbc_lblStartkapital.gridy = 0;
		panelLeft.add(lblStartkapital, gbc_lblStartkapital);

		JLabel lblZinsatz = new JLabel("Zinsatz");
		lblZinsatz.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblZinsatz = new GridBagConstraints();
		gbc_lblZinsatz.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblZinsatz.insets = new Insets(0, 0, 5, 0);
		gbc_lblZinsatz.gridx = 0;
		gbc_lblZinsatz.gridy = 1;
		panelLeft.add(lblZinsatz, gbc_lblZinsatz);

		JLabel lblLaufzeit = new JLabel("Laufzeit");
		lblLaufzeit.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblLaufzeit = new GridBagConstraints();
		gbc_lblLaufzeit.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLaufzeit.insets = new Insets(0, 0, 5, 0);
		gbc_lblLaufzeit.gridx = 0;
		gbc_lblLaufzeit.gridy = 2;
		panelLeft.add(lblLaufzeit, gbc_lblLaufzeit);

		JLabel lblEndkapital = new JLabel("Endkapital");
		lblEndkapital.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblEndkapital = new GridBagConstraints();
		gbc_lblEndkapital.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEndkapital.insets = new Insets(0, 0, 5, 0);
		gbc_lblEndkapital.gridx = 0;
		gbc_lblEndkapital.gridy = 3;
		panelLeft.add(lblEndkapital, gbc_lblEndkapital);

		rdbtnEndkapital.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_rdbtnEndkapital = new GridBagConstraints();
		gbc_rdbtnEndkapital.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnEndkapital.gridx = 0;
		gbc_rdbtnEndkapital.gridy = 4;
		panelLeft.add(rdbtnEndkapital, gbc_rdbtnEndkapital);
		rdbtnEndkapital.setSelected(true);

		JRadioButton rdbtnLaufzeit = new JRadioButton("Laufzeit");
		rdbtnLaufzeit.setBackground(Color.ORANGE);
		rdbtnLaufzeit.setMnemonic(KeyEvent.VK_F);
		rdbtnLaufzeit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				laufzeit.setEditable(false);
			}
		});

		JPanel flowHolder2 = new JPanel();
		flowHolder2.setBounds(225, 30, 200, 200);
		frame.getContentPane()
				.add(flowHolder2);
		flowHolder2.setBackground(Color.ORANGE);
		FlowLayout flowLayout_1 = (FlowLayout) flowHolder2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);

		JPanel panelRight = new JPanel();
		flowHolder2.add(panelRight);
		panelRight.setBackground(Color.ORANGE);
		GridBagLayout gbl_panelRight = new GridBagLayout();
		gbl_panelRight.columnWidths = new int[]{150};
		gbl_panelRight.rowHeights = new int[]{30, 30, 30, 30, 30};
		gbl_panelRight.columnWeights = new double[]{0.0};
		gbl_panelRight.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		panelRight.setLayout(gbl_panelRight);

		startkapital = new JTextField();
		startkapital.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				zinssatz.requestFocusInWindow();
			}
		});
		startkapital.setSize(new Dimension(150, 20));
		startkapital.setPreferredSize(new Dimension(150, 20));
		startkapital.setFont(new Font("Tahoma", Font.BOLD, 11));
		startkapital.setBorder(
				new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_startkapital = new GridBagConstraints();
		gbc_startkapital.fill = GridBagConstraints.HORIZONTAL;
		gbc_startkapital.insets = new Insets(0, 0, 5, 0);
		gbc_startkapital.gridx = 0;
		gbc_startkapital.gridy = 0;
		panelRight.add(startkapital, gbc_startkapital);
		startkapital.setColumns(10);

		laufzeit = new JTextField();
		laufzeit.setFont(new Font("Tahoma", Font.BOLD, 11));
		laufzeit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				endkapital.requestFocusInWindow();
			}
		});

		zinssatz = new JTextField();
		zinssatz.setFont(new Font("Tahoma", Font.BOLD, 11));
		zinssatz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				laufzeit.requestFocusInWindow();
			}
		});
		zinssatz.setSize(new Dimension(150, 20));
		zinssatz.setBorder(
				new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_zinssatz = new GridBagConstraints();
		gbc_zinssatz.fill = GridBagConstraints.HORIZONTAL;
		gbc_zinssatz.insets = new Insets(0, 0, 5, 0);
		gbc_zinssatz.gridx = 0;
		gbc_zinssatz.gridy = 1;
		panelRight.add(zinssatz, gbc_zinssatz);
		zinssatz.setColumns(10);
		laufzeit.setSize(new Dimension(150, 20));
		laufzeit.setBorder(
				new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_laufzeit = new GridBagConstraints();
		gbc_laufzeit.fill = GridBagConstraints.HORIZONTAL;
		gbc_laufzeit.insets = new Insets(0, 0, 5, 0);
		gbc_laufzeit.gridx = 0;
		gbc_laufzeit.gridy = 2;
		panelRight.add(laufzeit, gbc_laufzeit);
		laufzeit.setColumns(10);

		endkapital = new JTextField();
		endkapital.setFont(new Font("Tahoma", Font.BOLD, 11));
		endkapital.setSize(new Dimension(150, 20));
		endkapital.setBorder(
				new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_endkapital = new GridBagConstraints();
		gbc_endkapital.fill = GridBagConstraints.HORIZONTAL;
		gbc_endkapital.insets = new Insets(0, 0, 5, 0);
		gbc_endkapital.gridx = 0;
		gbc_endkapital.gridy = 3;
		panelRight.add(endkapital, gbc_endkapital);
		endkapital.setColumns(10);

		rdbtnLaufzeit.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_rdbtnLaufzeit = new GridBagConstraints();
		gbc_rdbtnLaufzeit.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnLaufzeit.gridx = 0;
		gbc_rdbtnLaufzeit.gridy = 4;
		panelRight.add(rdbtnLaufzeit, gbc_rdbtnLaufzeit);

		JPanel buttons = new JPanel();
		buttons.setBounds(0, 250, 434, 23);
		frame.getContentPane()
				.add(buttons);
		buttons.setLayout(new GridLayout(0, 3, 0, 0));

		JButton btnBerechnen = new JButton("Berechnen");
		buttons.add(btnBerechnen);
		btnBerechnen.setMnemonic(KeyEvent.VK_B);

		JButton btnLschen = new JButton("L\u00F6schen");
		buttons.add(btnLschen);
		btnLschen.setMnemonic(KeyEvent.VK_L);

		JButton btnBeenden = new JButton("Beenden");
		buttons.add(btnBeenden);
		btnBeenden.setMnemonic(KeyEvent.VK_E);

		JPanel tablePane = new JPanel();
		frame.getContentPane()
				.add(tablePane);
		tablePane.setBounds(new Rectangle(0, 280, 420, 170));
		GridBagLayout gbl_tablePane = new GridBagLayout();
		gbl_tablePane.columnWidths = new int[]{0, 0};
		gbl_tablePane.rowHeights = new int[]{0, 0};
		gbl_tablePane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_tablePane.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		tablePane.setLayout(gbl_tablePane);

		JScrollPane scrollPaneNew = new JScrollPane();
		GridBagConstraints gbc_scrollPaneNew = new GridBagConstraints();
		gbc_scrollPaneNew.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneNew.gridx = 0;
		gbc_scrollPaneNew.gridy = 0;
		tablePane.add(scrollPaneNew, gbc_scrollPaneNew);

		btnBeenden.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				JOptionPane pane = new JOptionPane();
				int dialogResult = JOptionPane.showConfirmDialog(null,
						"Beenden", "Wirklich beenden?",
						JOptionPane.YES_NO_OPTION);

				if (dialogResult == JOptionPane.YES_OPTION) {
					System.exit(0);
				}

			}
		});
		btnLschen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startkapital.setText("");
				zinssatz.setText("");
				laufzeit.setText("");
				endkapital.setText("");
				dataModel.setRowCount(0);
			}
		});

		btnBerechnen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Double wert = 0.0;
				NumberFormat nf = NumberFormat.getInstance();

				Object[] colNames = new Object[2];
				colNames[0] = "Jahr";
				colNames[1] = "Summe";
				Object[][] rowData;

				if (rdbtnEndkapital.isSelected() == true) {
					rdbtnLaufzeit.setSelected(false);
					wert = Zinsrechner.berechneEndkapaital(
							Double.parseDouble(startkapital.getText()),
							Double.parseDouble(zinssatz.getText()),
							Double.parseDouble(laufzeit.getText()));
					endkapital.setText(String.valueOf(nf.format(wert)));

					// Table erzeugen

					rowData = Zinsrechner.fillTable(
							Double.parseDouble(startkapital.getText()),
							Double.parseDouble(zinssatz.getText()),
							Double.parseDouble(laufzeit.getText()));

					dataModel = new DefaultTableModel(rowData, colNames);
					table.setModel(dataModel);
					scrollPaneNew.setViewportView(table);

				} else if (rdbtnLaufzeit.isSelected() == true) {
					rdbtnEndkapital.setSelected(false);
					wert = Zinsrechner.berechneLaufzeit(
							Double.parseDouble(startkapital.getText()),
							Double.parseDouble(zinssatz.getText()),
							Double.parseDouble(endkapital.getText()));

					rowData = Zinsrechner.fillTable(
							Double.parseDouble(startkapital.getText()),
							Double.parseDouble(zinssatz.getText()), wert);
					dataModel = new DefaultTableModel(rowData, colNames);
					table.setModel(dataModel);
					scrollPaneNew.setViewportView(table);

					laufzeit.setText(String.valueOf(nf.format(wert)));
				}
			}
		});

		ButtonGroup bg = new ButtonGroup();
		bg.add(rdbtnEndkapital);
		bg.add(rdbtnLaufzeit);

	}

}
