package vormittag.logic;

import java.text.NumberFormat;

public class Zinsrechner {

	public static double berechneEndkapaital(double startkapital,
			double zinssatz, double laufzeit) {

		return startkapital * Math.pow((1 + zinssatz / 100), laufzeit);

	}

	public static double berechneLaufzeit(double startkapital, double zinssatz,
			double endkapital) {
		return Math.log(endkapital / startkapital)
				/ Math.log(1.0 + zinssatz / 100.0);
	}

	public static Object[][] fillTable(double startkapital, double zinssatz,
			double laufzeit) {
		NumberFormat nf = NumberFormat.getInstance();
		Object[][] values = new Object[(int) laufzeit][2];

		for (int i = 0; i < values.length; i++) {
			values[i][0] = i + 1;
			values[i][1] = nf
					.format(berechneEndkapaital(startkapital, zinssatz, i + 1));
		}

		return values;
	}
}
