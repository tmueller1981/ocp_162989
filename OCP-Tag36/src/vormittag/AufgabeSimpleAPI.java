package vormittag;

public class AufgabeSimpleAPI {

	public static void main(String[] args) {

		// a1();
		// a2();
		// a3();
		a4();

		System.out.println("end of main");
	}

	static void a4() {

		for (int i = 0; i < 26; i++) {

			char ch = (char) ('A' + i);

			Runnable target = () -> {
				System.out.println(ch);
			};

			Thread th = new Thread(target);
			th.start();

		}
	}

	static void a3() {

		Runnable target = () -> {
			System.out.println(Thread.currentThread()
					.getId());
		};

		int countThreads = 37;
		Thread[] threads = new Thread[countThreads];
		for (int i = 0; i < countThreads; i++) {
			threads[i] = new Thread(target);
		}

		for (Thread th : threads) {
			th.start();
		}
	}

	static void a2() {

		Runnable target = () -> {
			while (true) {
				System.out.println(Thread.currentThread()
						.getId() + " "
						+ Thread.currentThread()
								.getName());
				pause(1000);
			}
		};

		Thread th = new Thread(target);
		th.start();
	}

	static void a1() {

		class MyThread extends Thread {
			@Override
			public void run() {
				while (true) {
					System.out.println(getId() + " " + getName());
					pause(1000);

				}
			}
		}

		Thread th = new MyThread();
		th.start();

	}

	static void pause(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}
}
