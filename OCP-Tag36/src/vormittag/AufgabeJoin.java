package vormittag;
import java.util.*;

public class AufgabeJoin {

	public static void main(String[] args) {

		List<Integer> list = new ArrayList<>();

		list = fillList(list);
		// for (Integer i : list)
		// System.out.println(i);
		System.out.println(list);
	}

	static List<Integer> fillList(List<Integer> list) {

		Runnable target = () -> {
			int min = -50;
			int max = 50;
			for (int i = 0; i < 100; i++) {
				list.add(new Random().nextInt((max - min) + 1) + min);
			}

		};
		Thread th = new Thread(target);
		th.start();
		try {
			th.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return list;
	}
}
