package vormittag;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 
 * @author Torben M�ller Folgendes Bsp. sollte vermieden werden
 *
 */
public class AufgabeSimpleRaceCondition {

	public static void main(String[] args) throws InterruptedException {

		List<Integer> list = new ArrayList<>();

		Runnable target = () -> {

			for (int i = 0; i < 1000; i++) {
				list.add(i);
			}
		};

		List<Thread> threads = Stream.generate(() -> new Thread(target))
				.limit(20)
				.collect(Collectors.toList());

		threads.forEach(Thread::start);
		threads.forEach(t -> {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		// Thread th1 = new Thread(target);
		// Thread th2 = new Thread(target);
		// th1.start();
		// th2.start();
		//
		// th1.join();
		// th2.join();

		System.out.println("list size: " + list.size());

	}
}
