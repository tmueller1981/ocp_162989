package vormittag;

public class AufgabeSimpleIncrement {

	volatile static int count;

	public static void main(String[] args) throws InterruptedException {

		Runnable target = () -> {
			for (int i = 0; i < 1_000_000; i++) {
				count++;
			}
		};

		ThreadGroup g = new ThreadGroup("main");
		Thread th = new Thread(g, target);
		Thread th2 = new Thread(g, target);

		th.start();
		th.join();
		th2.start();
		th2.join();

		System.out.println(count);
	}
}
