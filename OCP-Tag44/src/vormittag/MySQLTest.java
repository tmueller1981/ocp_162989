package vormittag;

import java.sql.*;
import java.util.Enumeration;
import java.util.Scanner;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

public class MySQLTest {

	static final String URL = "jdbc:mysql://localhost:3306/world?serverTimezone=Europe/Berlin";
	static final String USER = "root";
	static final String PASSWORD = "Torben!300581?#";

	public static void main(String[] args) {

		// listCountrys();
		// listCitys();
		// resultSetBewegen();
		// updateResultSet();
		// insertResultSet();
		deleteResultSet();
	}

	private static void deleteResultSet() {
		try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
				Statement stmt = conn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE)) {
			String sql = "select * from city where countrycode = 'USA'";
			ResultSet rs = stmt.executeQuery(sql);

			rs.last();
			System.out.println(rs.getRow());
			System.out.println(rs.getString("Name"));
			rs.deleteRow();
			System.out.println(rs.getRow());
			System.out.println(rs.getString("Name"));

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	static void insertResultSet() {
		try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
				Statement stmt = conn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE)) {
			String sql = "select * from city where countrycode = 'USA'";
			ResultSet rs = stmt.executeQuery(sql);
			rs.absolute(1);
			rs.moveToInsertRow();
			rs.updateString("Name", "Metropolis");
			rs.updateString("CountryCode", "USA");
			rs.updateString("District", "Metro");
			rs.updateInt("Population", 5_000_000);
			rs.insertRow();
			rs.moveToCurrentRow();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	static void updateResultSet() {
		try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
				Statement stmt = conn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE)) {
			String sql = "select * from city where countrycode = 'USA'";
			ResultSet rs = stmt.executeQuery(sql);
			rs.beforeFirst();
			while (rs.next()) {
				if (rs.getString("District")
						.equals("Utah")) {
					rs.updateInt("Population",
							rs.getInt("Population") - 10000000);
					rs.updateRow();
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	static void resultSetBewegen() {

		/**
		 * Ein ResultSet mit dem man mehr machen kann
		 */
		try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
				Statement stmt = conn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE)) {

			DatabaseMetaData dbMeta = conn.getMetaData();
			// System.out.println("Intensitive Ja/Nein: " + dbMeta
			// .supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE));
			// System.out.println("Forward Ja/Nein: " + dbMeta
			// .supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY));
			// System.out.println("Sensitive Ja/Nein: " + dbMeta
			// .supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE));
			//
			// System.out.println(
			// "Updateable: " + dbMeta.supportsResultSetConcurrency(
			// ResultSet.TYPE_SCROLL_INSENSITIVE,
			// ResultSet.CONCUR_UPDATABLE));

			String sql = "select * from city where countrycode = 'USA'";
			ResultSet rs = stmt.executeQuery(sql);
			/**
			 * Durch die Tabelle laufen
			 * 
			 * rs.next(); // zum ersten Datensatz
			 * System.out.println(rs.getString(2)); rs.next(); // zum zweiten
			 * Datensatz System.out.println(rs.getString(2)); rs.previous(); //
			 * eins zur�ck System.out.println(rs.getString(2)); rs.absolute(5);
			 * // eine absolute Zeile anspringen
			 * System.out.println(rs.getString(2)); rs.relative(2);
			 * System.out.println(rs.getString(2)); rs.last();
			 * System.out.println(rs.getString(2)); rs.absolute(-3); // dritter
			 * vom Ende System.out.println(rs.getString(2));
			 */

			rs.afterLast();
			while (rs.previous()) {
				System.out.println(rs.getString(2));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	static void listCountrys() {
		try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
				Statement stmt = conn.createStatement()) {
			System.out.println("Verbindung erfolgreich!\n");

			ResultSet rs = stmt.executeQuery(
					"SELECT Name, Continent, Population FROM country");

			System.out.printf("%-45s | %-15s | %15s %n", "Name", "Continent",
					"Population");

			while (rs.next()) {
				System.out.printf("%-45s | %-15s | %15s %n",
						rs.getString("Name"), rs.getString("Continent"),
						rs.getInt("Population"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	static void listCitys() {

		try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
				Statement stmt = conn.createStatement()) {

			Scanner sc = new Scanner(System.in);
			// System.out.println("Bitte SQL Statement eingeben:");
			// String statement = sc.next();

			ResultSet rs = stmt.executeQuery(
					"SELECT Name, Continent, Population FROM country");
			boolean resultSet = stmt.execute(
					"SELECT Country.Name, City.Name FROM Country JOIN City ON Country.Capital=City.ID ");
			// .execute("UPDATE city SET Population = Population - 1000");
			if (resultSet) {
				rs = stmt.getResultSet();
				java.sql.ResultSetMetaData rsMeta = rs.getMetaData();
				int columnCount = rsMeta.getColumnCount();
				System.out.println("ColumnCount: " + columnCount);

				int[] colSize = new int[columnCount];
				for (int i = 0; i <= columnCount - 1; i++) {
					colSize[i] = rsMeta.getColumnDisplaySize(i + 1) > rsMeta
							.getColumnName(i + 1)
							.length()
									? rsMeta.getColumnDisplaySize(i + 1)
									: rsMeta.getColumnName(i + 1)
											.length();
				}

				for (int i = 1; i <= columnCount; i++) {
					System.out.printf(" %-" + colSize[i - 1] + "s | ",
							rsMeta.getColumnName(i));
				}
				System.out.println();
				while (rs.next()) {
					for (int i = 1; i <= columnCount; i++) {
						System.out.printf(" %-" + colSize[i - 1] + "s | ",
								rs.getString(i));
					}
					System.out.println();
				}
			} else {
				int count = stmt.getUpdateCount();
				if (count == 0) {
					System.out.println("Keine Zeile betroffen");
				} else {
					System.out.println(count + " Zeilen betroffen");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
