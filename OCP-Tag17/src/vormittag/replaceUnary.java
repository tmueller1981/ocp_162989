package vormittag;
import java.util.*;

public class replaceUnary {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(1,2,3,4,5,6);
		list.replaceAll(i -> i+3);
		System.out.println(list);

//		list.replaceAll(i -> { if(i%2 != 0) return 0; else return i; }); // alternative
		list.replaceAll(i -> i % 2 != 0 ? 0 : i);
		System.out.println(list);
	}
}
