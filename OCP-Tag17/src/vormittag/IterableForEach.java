package vormittag;
import java.util.*;

public class IterableForEach {

	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		list.add(11);
		list.add(13);
		list.add(25);
		list.add(30);
		list.add(100);
		
		list.forEach(i -> System.out.println(i));
	}
}
