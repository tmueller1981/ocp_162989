package vormittag;
import java.util.*;
import java.util.Comparator;
import java.util.function.Predicate;

public class CollRemoveIf {

	public static void main(String[] args) {
		
		Collection<String> coll = Arrays.asList("mo","di","mi","do","fr");
		Predicate<String> filter = s -> s.startsWith("m");
		coll = new ArrayList<>(coll);
		System.out.println(coll);
		coll.removeIf(filter);
		System.out.println(coll);
		coll.forEach(System.out::println);
		

	}
}
