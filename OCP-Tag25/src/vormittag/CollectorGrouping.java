package vormittag;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import words.Words;

public class CollectorGrouping {

	public static void main(String[] args) {

		a1();
		a2();
	}

	private static void a2() {

		// Map<Laenge, Anzahl>
	}

	private static void a1() {
		System.out.println("*** Aufgabe 01");

		Function<String, Integer> classifier = x -> {
			return x.length();
		};

		Collector<String, ?, Map<Integer, List<String>>> c1 = Collectors
				.groupingBy(classifier);
		Map<Integer, List<String>> groups = Words.passwords()
				.stream()
				.collect(c1);

		for (Integer group : groups.keySet()) {
			System.out.println("PW L�nge: " + group);
			List<String> list = groups.get(group);
			System.out.println("W�rter: " + list);
		}

	}
}
