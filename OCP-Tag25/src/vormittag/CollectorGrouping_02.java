package vormittag;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CollectorGrouping_02 {

	public static void main(String[] args) {

		a1();
	}

	private static void a1() {

		class Tier implements Comparable<Tier> {
			String name;
			int alter;

			public Tier(String name, int alter) {
				this.name = name;
				this.alter = alter;
			}

			@Override
			public String toString() {
				return name + " " + alter;
			}

			@Override
			public int compareTo(Tier o) {
				int erg = name.compareTo(o.name);
				return erg != 0 ? alter : alter - o.alter;
			}
		}

		Tier[] array = {new Tier("Tom", 2), new Tier("Jerry", 7),
				new Tier("Bello", 3), new Tier("Rex", 5)

		};

		Function<Tier, String> classifier = x -> {
			return x.alter < 4 ? "jung" : "alt";
		};
		Collector<Tier, ?, TreeSet<Tier>> downstream = Collectors
				.toCollection(TreeSet::new);
		Collector<Tier, ?, Map<String, TreeSet<Tier>>> c1 = Collectors
				.groupingBy(classifier, downstream);

		Map<String, TreeSet<Tier>> groups = Arrays.stream(array)
				.collect(c1);
		System.out.println(groups);
	}
}
