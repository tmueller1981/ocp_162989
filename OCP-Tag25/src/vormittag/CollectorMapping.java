package vormittag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CollectorMapping {

	public static void main(String[] args) {
		a1();
		a2();
	}

	static void a1() {
		System.out.println("*** Aufgabe 01");
		String[] array = {"Mo", "Di", "Mi"};

		Function<String, String> mapper = s -> s.toUpperCase();
		Collector<String, ?, List<String>> downstream = Collectors.toList();

		Collector<String, ?, List<String>> collector = Collectors
				.mapping(mapper, downstream);

		List<String> list = Arrays.stream(array)
				.collect(collector);

		System.out.println(list);

	}

	static void a2() {
		System.out.println("\n*** Aufgabe 02");

		String[] array = {"de", "pl", "fr", "en"};

		Supplier<ArrayList<Locale>> collectionFactory = ArrayList::new;
		Collector<Locale, ?, ArrayList<Locale>> downstream = Collectors
				.toCollection(collectionFactory);
		Function<String, Locale> mapper = Locale::new;
		Collector<String, ?, ArrayList<Locale>> coll = Collectors
				.mapping(mapper, downstream);

		ArrayList<Locale> list = Arrays.stream(array)
				.collect(coll);

		// in einem Aufruf
		list = Arrays.stream(array)
				.collect(Collectors.mapping(Locale::new,
						Collectors.toCollection(ArrayList::new)));

		list.forEach(loc -> {
			System.out.println("Locales: " + loc + " Sprache: "
					+ loc.getDisplayLanguage());
		});
	}
}
