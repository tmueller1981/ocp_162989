package nachmittag;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Person implements Comparable<Person> {
	private String name;
	private String beruf;

	public Person(String name, String beruf) {
		this.name = name;
		this.beruf = beruf;
	}
	// more methods here...
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Person o) {
		int erg = this.name.compareTo(o.name);
		return erg != 0 ? erg : this.beruf.compareTo(o.beruf);
	}

	public static void main(String[] args) {

		Person[] personen = {new Person("Tom", "Bauarbeiter(in)"),
				new Person("Jerry", "Lehrer(in)"),
				new Person("Peter", "Metzger(in)"),
				new Person("Paul", "Bauarbeiter(in)"),
				new Person("Mary", "Lehrer(in)"),};

		a1(personen);
		a2(personen);
		a3(personen);
		a4(personen);
	}
	static void a4(Person[] personen) {
		System.out.println("\n*** Aufgabe 04");

		Predicate<Person> predicate = p -> "Bauarbeiter(in)".equals(p.beruf);
		Collector<Person, ?, Map<Boolean, List<Person>>> coll = Collectors
				.partitioningBy(predicate);

		Map<Boolean, List<Person>> result = Arrays.stream(personen)
				.collect(coll);
		for (Map.Entry<Boolean, List<Person>> entry : result.entrySet()) {
			System.out.println(entry.getKey()
					? "Bauarbeiter(in): "
					: "Kein(e) Bauarbeiter(in)");
			System.out.println(entry.getValue());
		}
	}
	static void a3(Person[] personen) {
		System.out.println("\n*** Aufgabe 03");

		Collector<Person, ?, Collection<String>> c1 = Collectors
				.mapping((p) -> p.beruf, Collectors.toCollection(HashSet::new));
		Collection<String> berufe = Arrays.stream(personen)
				.collect(c1);

		System.out.println("Berufe: " + berufe);
	}

	static void a2(Person[] personen) {
		System.out.println("\n*** Aufgabe 02");

		Function<Person, String> classifier = (p) -> p.beruf;
		Collector<Person, ?, Map<String, List<Person>>> coll = Collectors
				.groupingBy(classifier);
		Map<String, List<Person>> setPerson = Arrays.stream(personen)
				.collect(coll);
		for (String p : setPerson.keySet()) {
			List<Person> list = setPerson.get(p);
			System.out.println(p);
			System.out.println(list);
		}
	}

	static void a1(Person[] personen) {
		System.out.println("*** Aufgabe 01");

		Collector<Person, ?, TreeSet<Person>> coll = Collectors
				.toCollection(TreeSet::new);
		TreeSet<Person> setPerson = Arrays.stream(personen)
				.collect(coll);
		System.out.println(setPerson);
	}
}
