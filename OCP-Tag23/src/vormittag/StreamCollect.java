package vormittag;

import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;

public class StreamCollect {

	public static void main(String[] args) {
		// a1();
		// a2();
		// a3();
		a4();
	}

	static void a1() {
		String[] array = {"a", "b", "c", "d"};

		/*
		 * 
		 * Parameter f�r stream.collect()
		 * 
		 * Supplier<HashSet<String>> sup = () -> new HashSet<>();
		 * BiConsumer<HashSet<String>, String> acc = (set, value) ->
		 * set.add(value); BiConsumer<HashSet<String>, HashSet<String>> cmb =
		 * (set1, set2) -> set1.addAll(set2);
		 */

		HashSet<String> bigSet = Arrays.stream(array)
				.collect(HashSet::new, HashSet::add, HashSet::addAll); // Methodenreferenz
		System.out.println(bigSet);
	}

	static void a2() {
		Integer[] arr1 = {1, 2, 3};
		System.out.println("Sum: " + Arrays.stream(arr1)
				.reduce(0, (i1, i2) -> i1 + i2));

		AtomicInteger[] arr2 = {new AtomicInteger(1), new AtomicInteger(2),
				new AtomicInteger(3)};

		BinaryOperator<AtomicInteger> operator = (i1, i2) -> {
			i1.addAndGet(i2.get());
			return i1;
		};

		System.out.println("Sum2: " + Arrays.stream(arr2)
				.reduce(new AtomicInteger(0), operator));
	}

	static void a3() {
		Integer[] arr1 = {1, 2, 3};

		Supplier<AtomicInteger> supplier = AtomicInteger::new;
		BiConsumer<AtomicInteger, AtomicInteger> combiner = (AtomicInteger i1,
				AtomicInteger i2) -> {
			i1.addAndGet(i2.get());
		};
		BiConsumer<AtomicInteger, Integer> acc = (a1, value) -> {
			a1.addAndGet(value);
		};

		AtomicInteger sum = Arrays.stream(arr1)
				.collect(supplier, acc, combiner);
		System.out.println(sum);
	}

	static void a4() {
		String[] array = {"a", "b", "c", "d"};

		Supplier<StringBuilder> sup = StringBuilder::new;
		BiConsumer<StringBuilder, String> acc = (sb, value) -> {
			sb.append(value);
		};
		BiConsumer<StringBuilder, StringBuilder> cmb = (sb1, sb2) -> sb1
				.append(sb2.toString());

		StringBuilder sb = Arrays.stream(array)
				.collect(sup, acc, cmb);
		System.out.println(sb.toString() + " " + sb.getClass());
	}
}
