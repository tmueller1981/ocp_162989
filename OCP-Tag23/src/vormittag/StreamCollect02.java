package vormittag;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Locale;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamCollect02 {

	public static void main(String[] args) {
		// a1();
		a2();
	}

	static void a1() {
		String[] array = {"heute", "ist", "Freitag"};

		Supplier<ArrayDeque<String>> sup = ArrayDeque::new;
		Collector<String, ?, ArrayDeque<String>> collector = Collectors
				.toCollection(sup);

		ArrayDeque<String> ad = Arrays.stream(array)
				.collect(collector);
		System.out.println(ad.toString());
	}

	static void a2() {
		Locale[] locales = Locale.getAvailableLocales();
		Collector<String, ?, TreeSet<String>> col = Collectors
				.toCollection(TreeSet::new);

		TreeSet<String> ts = Arrays.stream(locales)
				.map(s -> s.getDisplayCountry())
				.collect(col);

		System.out.println(ts.toString());
		// for (String string : ts) {
		// System.out.println(string);
		// }
	}
}
