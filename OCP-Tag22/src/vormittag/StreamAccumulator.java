package vormittag;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamAccumulator {

	public static void main(String[] args) {
		// a1();
		// a2();
		a3();
	}

	static void a1() {
		String[] items = {"Java", " ", "ist", " ", "toll"};
		String str = Arrays	.stream(items)
							.reduce((s, s1) -> s + s1)
							.get();
		System.out.println(str);
	}
	static void a2() {
		Integer sum = Stream.of(2, 3, 2, 3)
							.reduce((i, i1) -> i * i1)
							.get();
		System.out.println(sum);
	}

	static void a3() {
		String[] items = {"aa", "bbb", "cccc", "ddddd"};
		int leng = Arrays	.stream(items)
							.reduce((s, s1) -> s + s1)
							.get()
							.length();
		System.out.println(leng);
	}
}
