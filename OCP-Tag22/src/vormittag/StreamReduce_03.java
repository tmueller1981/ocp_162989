package vormittag;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class StreamReduce_03 {

	public static void main(String[] args) {
		// a1();
		a2();
	}

	static void a1() {
		Integer[] values = {1, 2, 3, 4};

		Integer identity = 0;
		BiFunction<Integer, Integer, Integer> accumulator = (a, b) -> a + b;
		BinaryOperator<Integer> combiner = (a, b) -> {
			return a + b;
		};

		Integer sum = Arrays.stream(values)
							.reduce(identity, accumulator, combiner);

		System.out.println(sum);
	}

	static void a2() {
		String[] items = {"aa", "bbb", "cccc", "ddddd"};
		String identity = "";
		BiFunction<String, String, String> accumulator = (a, b) -> a + b;
		BinaryOperator<String> combiner = (a, b) -> {
			return a + b;
		};

		String sum = Arrays	.stream(items)
							.reduce(identity, accumulator, combiner);

		System.out.println("Gesamte Stringlšnge: " + sum.length());
	}
}
