package nachmittag;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;
public class OwnStringBuilder {

	static void aufgabe01() {
		/*
		 * Achtung! So sollte die reduce nicht verwendet werden!
		 */
		String[] array = {"a", "b", "c", "d"};

		StringBuilder idt = new StringBuilder();
		BiFunction<StringBuilder, String, StringBuilder> acc = new BiFunction<StringBuilder, String, StringBuilder>() {
			@Override
			public StringBuilder apply(StringBuilder t, String u) {
				return t.append(u);
			}
		};

		BinaryOperator<StringBuilder> comb = (sBuilder1,
				sBuilder2) -> sBuilder1;

		StringBuilder result = Stream	.of(array)
										.reduce(idt, acc, comb);

		System.out.println("result: " + result);
	}

	static void aufgabe02() {
		String[] array = {"a", "b", "c", "d"};

		StringBuilder idt = new StringBuilder();
		BiFunction<StringBuilder, String, StringBuilder> acc = new BiFunction<StringBuilder, String, StringBuilder>() {
			@Override
			public StringBuilder apply(StringBuilder t, String u) {
				return new StringBuilder(t).append(u);
			}
		};

		BinaryOperator<StringBuilder> comb = (sBuilder1,
				sBuilder2) -> sBuilder1.append(sBuilder2);

		StringBuilder result = Stream	.of(array)
										.reduce(idt, acc, comb);

		System.out.println("result: " + result);
	}
}
