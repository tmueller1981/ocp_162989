package nachmittag;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

class Person {
	String vorname, nachname;

	public Person(String vorname, String nachname) {
		this.nachname = nachname;
		this.vorname = vorname;

	}

	@Override
	public String toString() {
		return this.vorname + " " + this.nachname;
	}
}

public class Personen {

	public static void main(String[] args) {

		aufgabe1();
	}

	static String max(String s1, String s2) {
		return s1.compareTo(s2) > 0 ? s1 : s2;
	}

	static void aufgabe1() {
		List<Person> personen = new ArrayList<Person>();
		personen.add(new Person("Tom", "Katze"));
		personen.add(new Person("Jerry", "Maus"));
		personen.add(new Person("Alexander", "Poe"));

		BinaryOperator<Person> accumulator = (p1, p2) -> {
			String vorname = max(p1.vorname, p2.vorname);
			String nachname = max(p1.nachname, p2.nachname);
			return new Person(vorname, nachname);
		};

		Person person1 = personen.stream()
				.reduce(accumulator)
				.get();

		Person identity = new Person("", "");

		Person person2 = personen.stream()
				.reduce(identity, accumulator);

		BiFunction<Person, Person, Person> acc2 = accumulator;
		BinaryOperator<Person> cmb = accumulator;

		Person person3 = personen.stream()
				.reduce(identity, acc2, cmb);

		System.out.println("Ergebnis 1: " + person1);
		System.out.println("Ergebnis 2: " + person2);
		System.out.println("Ergebnis 3: " + person3);
	}
}
