package nachmittag;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

class TextStatisticsCombiner extends TextStatistics {

	private TextStatistics stats1, stats2;

	public TextStatisticsCombiner(TextStatistics stats1,
			TextStatistics stats2) {
		super(null);
		this.stats1 = stats1;
		this.stats2 = stats2;
	}

	@Override
	public int getCountChars() {
		return stats1.getCountChars() + stats2.getCountChars();
	}

	@Override
	public int getCountLetters() {
		return stats1.getCountLetters() + stats2.getCountLetters();
	}

	@Override
	public int getCountSpecialChars() {
		return stats1.getCountSpecialChars() + stats2.getCountSpecialChars();
	}

	@Override
	public Optional<String> getLongestWord() {
		Optional<String> op1 = stats1.getLongestWord();
		Optional<String> op2 = stats2.getLongestWord();

		if (op1.isEmpty() && op2.isEmpty()) {
			return op1;
		}
		if (op1.isEmpty()) {
			return op2;
		}
		if (op1.get()
				.length() > op2.get()
						.length()) {
			return op1;
		}
		return op2;
	}
}

public class TextStatistics {
	String text;

	public TextStatistics(String text) {
		this.text = text;
	}

	public int getCountChars() {
		// String[] words = this.text.split(" ");
		// return Arrays .stream(words)
		// .count();
		return this.text.length();

	}
	public int getCountSpecialChars() {
		// String[] words = this.text.split(" ");
		// return Arrays.stream(words)
		// .filter(s -> s.contains("�") || s.contains("�")
		// || s.contains("�"))
		// .count();

		return (int) this.text.chars()
				.filter(i -> !Character.isLetterOrDigit((char) i))
				.count();

	}
	public int getCountLetters() {
		// String[] words = this.text.split(" ");
		// return Arrays.stream(words)
		// .map(s1 -> s1.length())
		// .map(s -> s.intValue())
		// .reduce((i1, i2) -> i1 + i2)
		// .get();

		return (int) this.text.chars()
				.filter(Character::isLetter)
				.count();

	}
	public Optional<String> getLongestWord() {
		String[] words = this.text.split(" ");
		return Arrays.stream(words)
				// .sorted((s1, s2) -> s1.compareTo(s2))
				.max((Comparator.comparing(String::length)));

	}

	public static void main(String[] args) {

		TextStatistics stats = new TextStatistics(
				"Um die W�rte im Text zu finden, d�rfen Sie zur Vereinfachung davon ausgehen, dass die einzelnen W�rter voneinander mit Leerzeichen getrennt werden und andere Sonderzeichen beinhalten k�nnen (oder komplett aus Sonderzeichen bestehen k�nnen). Dann wird folgende Zeile ein Array aus einzelnen W�rtern erstellen:");

		System.out.printf(
				"Der Text enth�lt: %n %n Zeichen: %s %n Buchstaben: %s %n Sonderzeichen: %s %n L�ngstes Wort: %s",
				stats.getCountChars(), stats.getCountLetters(),
				stats.getCountSpecialChars(), stats.getLongestWord());

		/**
		 * Aufgabe 2
		 */
		String[] input = {"java ist toll", "streams sind einfacher"};

		TextStatistics idt = new TextStatistics("");
		BiFunction<TextStatistics, String, TextStatistics> acc = (stats2,
				str) -> {
			TextStatistics stats3 = new TextStatistics(str);
			return new TextStatisticsCombiner(stats2, stats3);
		};
		BinaryOperator<TextStatistics> cmb = (stats2,
				stats3) -> new TextStatisticsCombiner(stats2, stats3);

		TextStatistics stats2 = Arrays.stream(input)
				.reduce(idt, acc, cmb);

		System.out.println("\n\n++++++++++");
		System.out.println(stats2.getLongestWord()
				.get());
		System.out.println(stats2.getCountChars());

		int countChars = Arrays.stream(input)
				.map(String::length)
				.reduce(0, (a, b) -> a + b);
		System.out.println(countChars);
	}
}
