package vormittag;

class MyThread extends Thread {
	@Override
	public void run() {
		System.out.println(this.getId() + " " + this.getName());
	}
}

class MyRunnable implements Runnable {
	@Override
	public void run() {
		System.out.println(Thread.currentThread()
				.getId() + " "
				+ Thread.currentThread()
						.getName());

	}
}

public class ThreadTest {

	public static void main(String[] args) {

		// a1();
		a2();
	}

	static void a1() {

		while (true) {
			new MyThread().start();
			try {
				MyThread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	static void a2() {
		Runnable target = new MyRunnable();

		while (true) {
			new Thread(target).start();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
