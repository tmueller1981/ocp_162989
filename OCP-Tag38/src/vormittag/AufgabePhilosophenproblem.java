package vormittag;

class Philosoph extends Thread {
	private String name;
	Object links, rechts;

	public Philosoph(String name) {
		this.name = name;
	}

	void setLinks(Object links) {
		this.links = links;
	}
	void setRechts(Object rechts) {
		this.rechts = rechts;
	}

	@Override
	public void run() {

		while (true) {
			tagesAblauf();
		}
	}

	/**
	 * L�sung ergibt Dead-Lock
	 * 
	 * Kann mit dem Interface look gel�st werden
	 */

	public void tagesAblauf() {
		try {
			System.out.println(this.name + " denkt nach...");
			Thread.sleep(2000);
			System.out.println(this.name + " bekommt Hunger");
			synchronized (rechts) {
				System.out.println(this.name + " nimmt die rechte Gabel");
				synchronized (links) {

					System.out.println(this.name + " nimmt die linke Gabel");
					System.out.println(this.name + " isst...");
					System.out.println(this.name + " legt die linke Gabel ab");

				}
				System.out.println(this.name + " legt die rechte Gabel ab");
			}

			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

public class AufgabePhilosophenproblem {

	public static void main(String[] args) {

		Object links = new Object();
		Object rechts = new Object();

		Philosoph p1 = new Philosoph("Sokrates");
		p1.setLinks(links);
		p1.setRechts(rechts);
		p1.start();

		Philosoph p2 = new Philosoph("Platon");
		p2.setLinks(rechts);
		p2.setRechts(links);
		p2.start();

		Philosoph p3 = new Philosoph("Dante");
		p3.setLinks(links);
		p3.setRechts(rechts);
		p3.start();
	}
}
