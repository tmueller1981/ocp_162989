package vormittag;
import java.util.*;

class Baeckerei extends Thread {
	private String name;
	private final int KAPAZITAET;
	private ArrayList<Integer> lager;

	public Baeckerei(String name, int lager) {
		this.name = name;
		this.KAPAZITAET = lager;
		this.lager = new ArrayList<>();
	}

	public ArrayList<Integer> getLager() {
		return this.lager;
	}

	@Override
	public void run() {

		while (true) {
			if (lager.size() < KAPAZITAET) {
				System.out.println("10 Br�tchen backen");
				synchronized (lager) {
					for (int i = 0; i < 10; i++) {
						lager.add(i + 1);
					}
				}

			} else {
				int dif = KAPAZITAET - lager.size();
				System.out.println(dif + " Br�tchen backen");
				synchronized (lager) {
					for (int i = 0; i < dif; i++) {
						lager.add(i + 1);
					}
				}

			}
			try {
				System.out.println(this.lager.size() + " Br�tchen im Lager");
				Thread.sleep(500);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized (this) {
				this.notify();
			}

		}
	}

}

class Person extends Thread {
	private String name;
	private Baeckerei baeckerei;
	private int menge;

	public Person(String name, Baeckerei baekcerei) {
		this.name = name;
		this.baeckerei = baekcerei;
	}

	@Override
	public void run() {

		while (true) {
			menge = new Random().nextInt(15 - 3) + 3;
			int menge2 = baeckerei.getLager()
					.size() < menge
							? baeckerei.getLager()
									.size()
							: menge;
			try {
				synchronized (baeckerei) {
					// Auf Baeckerei warten
					baeckerei.wait();
					System.out.println(name + " geht einkaufen.");
					synchronized (baeckerei.getLager()) {
						for (int i = 0; i < menge2; i++) {
							baeckerei.getLager()
									.remove(i);
						}
					}

				}
				System.out.println(
						name + " hat " + menge2 + " Br�tchen gekauft.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}

public class ThreadsBaeckerei {

	public static void main(String[] args) {

		Baeckerei b1 = new Baeckerei("Agethen", 100);

		Person p1 = new Person("Mike", b1);
		Person p2 = new Person("Trudhy", b1);
		Person p3 = new Person("Alex", b1);

		b1.start();
		p1.start();
		p2.start();
		p3.start();
	}
}
