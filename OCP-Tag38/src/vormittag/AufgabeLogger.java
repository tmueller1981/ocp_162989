package vormittag;

class MyLogger {
	// private StringBuffer sb = new StringBuffer();
	private StringBuilder sb = new StringBuilder();

	public void addMessage(String caller, String message) {

		synchronized (this) {
			sb.append(caller)
					.append(" - ")
					.append(message)
					.append("\n");
		}
	}

	public String getLog() {
		return sb.toString();
	}
}

public class AufgabeLogger {

	public static void main(String[] args) throws InterruptedException {

		MyLogger logger = new MyLogger();

		Runnable target = () -> {
			String threadName = Thread.currentThread()
					.getName();
			for (int i = 0; i < 100; i++) {
				logger.addMessage(threadName, "Nachricht: " + i);
			}
		};

		Thread t1 = new Thread(target);
		Thread t2 = new Thread(target);

		t1.start();
		t2.start();

		t1.join();
		t2.join();

		System.out.println(logger.getLog());
	}
}
