package nachmittag;

import java.util.Objects;

public abstract class Auto {

	private int baujahr;
	private String modell;
	
	public Auto(String modell, int baujahr) {
		this.modell = modell;
		this.baujahr = baujahr;
	}
	
	public String getModell() {
		return this.modell;
	}
	
	public int getBaujahr() {
		return this.baujahr;
	}
	
	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	/**
	 * ‹berschreiben um einen eigenen Hash-Bucket zu genereiren
	 */
	@Override
	public int hashCode() {
		return Objects.hash(modell) + baujahr;
	}
	
	public boolean equals(Object obj) {
		if( !(obj instanceof Auto)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		Auto a = (Auto) obj;
		return Objects.equals(modell, a.modell) && baujahr == a.baujahr;
	}
	/* Hash-Bucket Ende */
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " Modell: " + this.getModell() + ", Baujahr " + this.getBaujahr();
	}
}
