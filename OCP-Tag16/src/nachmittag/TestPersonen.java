package nachmittag;
import java.util.*;

class Person {
	public String vorname;
	public String nachname;
	
	public Person(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}
	
	@Override
	public String toString() {
		return "Person: " + vorname + " " + nachname;
	}
}

public class TestPersonen {

	public static void main(String[] args) {
		
		Person[] pers = {
				new Person("Paul", "Smith"),
				new Person("Paul", "Black"),
				new Person("John", "Smith"),
				new Person("John", "Black")
			};
			
			List<Person> listPers = new ArrayList<Person>();
			listPers.add(new Person("Paul", "Smith"));
			listPers.add(new Person("Paul", "Black"));
			listPers.add(new Person("John", "Smith"));
			listPers.add(new Person("John", "Black"));
			
			
			for(int i =0; i < pers.length; i++) {
				System.out.println(pers[i]);
			}
			for (Person person : listPers) {
				System.out.println(person);
			}
			Comparator<Person> nachname = (p1, p2) -> p1.nachname.compareTo(p2.nachname);
			Comparator<Person> vorname = (p1, p2) -> p1.vorname.compareTo(p2.vorname);
			Arrays.sort(pers, nachname.thenComparing(vorname));
			Collections.sort(listPers, nachname.thenComparing(vorname));
			System.out.println("\n");
			for(int i =0; i < pers.length; i++) {
				System.out.println(pers[i]);
			}
			System.out.println("\n");
			for (Person person : listPers) {
				System.out.println(person);
			}
			
			nachname = (p1, p2) -> p2.nachname.compareTo(p1.nachname);
			vorname = (p1, p2) -> p2.vorname.compareTo(p1.vorname);
			Arrays.sort(pers, nachname.thenComparing(vorname));
			Collections.sort(listPers, nachname.thenComparing(vorname));
			System.out.println("\n");
			for(int i =0; i < pers.length; i++) {
				System.out.println(pers[i]);
			}
			System.out.println("\n");
			for (Person person : listPers) {
				System.out.println(person);
			}
	}
	
}
