package nachmittag;
import java.util.*;

public class AutoApp {

	public static void main(String[] args) {
		
		VW vw1 = new VW("Golf", 1990);
		BMW bmw1 = new BMW("Z4", 2000);
		
		System.out.println("Aufgabe 2 **********");
		System.out.println(vw1);
		System.out.println(bmw1);
		
		vw1 = new VW("Golf", 1990);
		VW vw2 = new VW("Bully", 2015);
		VW vw3 = new VW("Golf", 2003);
		
		System.out.println("\nAufgabe 3 **********\n");
		System.out.println(vw1);
		System.out.println(vw2);
		System.out.println(vw3);
		
		Comparator<Auto> cmp = (o1, o2) -> o1.getModell().compareTo(o2.getModell());
		Comparator<Auto> cmpR = (a1, a2) -> a2.getModell().compareTo(a1.getModell());
		Comparator<Auto> cmp2 = new Comparator<Auto>() {
			
			@Override
			public int compare(Auto o1, Auto o2) {
				if(o1.getBaujahr() < o2.getBaujahr()) {
					return -1;
				} else {
					return 0;
				}
			}
		};	
		
		List<VW> autosLL = new LinkedList<>();
		autosLL.add(vw1);
		autosLL.add(vw2);
		autosLL.add(vw3);
		autosLL.sort(cmp.thenComparing(cmp2));
		System.out.println("\nAufgabe 5 LL *******\n");
		printList(autosLL);
		
		Set<VW> autoHS = new HashSet<VW>(autosLL);
		System.out.println("\nAufgabe 5 HS *******\n");
		autoHS.add(new VW("Golf", 2003));
		printList(autoHS);
	
		Set<VW> autoTS = new TreeSet<VW>(cmp.thenComparing(cmp2));
		autoTS.addAll(autosLL);
		System.out.println("\nAufgabe 5 TS *******\n");
		printList(autoTS);
		
		Queue<VW> autoPQ = new PriorityQueue<VW>(cmp.thenComparing(cmp2));
		autoPQ.addAll(autosLL);
		System.out.println("\nAufgabe 5 PQ *******\n");
		printList(autoPQ);
				
		BMW bmw2 = new BMW("X3", 2010);
		BMW bmw3 = new BMW("X5", 2010);
		
		List<BMW> arrayL = new ArrayList<BMW>();
		arrayL.add(bmw1);
		arrayL.add(bmw2);
		arrayL.add(bmw3);
		arrayL.sort(cmp.thenComparing(cmp2));
		System.out.println("\nAufgabe 6 AL *******\n");
		printList(arrayL);
		
		Set<BMW> hashAuto = new HashSet<BMW>(arrayL);
		System.out.println("\nAufgabe 6 HS *******\n");
		printList(hashAuto);
		
		Set<BMW> treeAuto = new TreeSet<BMW>(cmp.thenComparing(cmp2));
		treeAuto.addAll(arrayL);
		System.out.println("\nAufgabe 6 TS *******\n");
		printList(treeAuto);
		
		Deque<BMW> dequeAuto = new ArrayDeque<BMW>(arrayL);
		System.out.println("\nAufgabe 6 AD *******\n");
		printList(dequeAuto);
		
		System.out.println("\nAufgabe 7 *******\n");
		System.out.println(hashAuto.contains(bmw1));
		System.out.println(bmw1);
		
		bmw1.setBaujahr(1999);
		System.out.println("\nAufgabe 8 *******\n");
		System.out.println(hashAuto.contains(bmw1));
		System.out.println(bmw1);
		
		autosLL.add(new VW("Polo", 2200));
		System.out.println("\nAufgabe 9 *******\n");
		printList(autosLL);
		
		System.out.println("\nAufgabe 10 *******\n");
		int index = Collections.binarySearch(autosLL, new VW("Polo", 2200), cmp.thenComparing(cmp2));
		System.out.println("Index von VW Polo 2200: " + index);
		
		Collections.sort(autosLL, cmpR);
		System.out.println("\nAufgabe 12 *******\n");
		printList(autosLL);
		
		System.out.println("\nAufgabe 14 *******\n");
		int index2 = Collections.binarySearch(autosLL, new VW("VW Polo", 3300), cmp.thenComparing(cmp2));
		System.out.println("Index von VW Polo 3300: " + index2);
		printList(autosLL);
	}
	
	static void printList(Collection<? extends Auto> c) {
		for (Auto auto : c) {
			System.out.println(auto);
		}
	}
}
