package nachmittag;
import java.util.*;

class myList  {
	
	static List<String> myList = new LinkedList<String>();
	
	void add(String s) {
		if(myList.size() == 10)
			throw new IllegalArgumentException("Menge 10 ist überschritten");
		else
			myList.add(s);
	}
	
	int size() {
		return myList.size();
	}
	
	String get(int index) {
		if(index < 0 || index > 9)
			throw new IllegalArgumentException("Falscher Index");
		return myList.get(index);
	}
	
	@Override
	public String toString() {
		return myList.toString();
	}

//	@Override
//	public Iterator iterator() {
//		return myList.listIterator();
//	}
}


public class ListIterable {
	
	public static void main(String[] args) {
		
		myList myList = new myList();
		
		for(int i = 0; i < 10; i++) {
			myList.add("Irgend ein Satz " +i);
		}
		System.out.println(myList);
		System.out.println("************");
		for (String string : myList.myList) {
			System.out.println(string);
		}
		
	}
}
