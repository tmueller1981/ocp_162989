package vormittag;
import java.time.*;
import java.util.*;

public class MapDate {

	public static void main(String[] args) {
		
		Map<Integer, LocalDate> mapDate = new HashMap<>();
		mapDate.put(1, LocalDate.of(2019, 07, 03));
		mapDate.put(2, LocalDate.of(2019, 06, 03));
		mapDate.put(3, LocalDate.of(2019, 05, 03));
		mapDate.put(4, LocalDate.of(2019, 04, 03));
		
		Map.Entry<Integer, LocalDate> entry;
		Set<Map.Entry<Integer, LocalDate>> set = mapDate.entrySet();
		
		for (Map.Entry<Integer, LocalDate> entry2 : set) {
			System.out.println(entry2);
		}
		
	}
}
