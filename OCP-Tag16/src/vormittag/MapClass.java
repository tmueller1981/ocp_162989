package vormittag;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapClass {

	public static void main(String[] args) {
		
		Map<Integer, String> map = new HashMap<>();
		
		for(int i = 1; i < 11; i++) {
			map.put(i, "Wort "+i);
		}
		
		System.out.println(map.get(5)); // 5ter Wert aus der HashMap
		
		map.put(5, "Neuer Wert"); // Gleichzeitig liefert put() den alten Wert zurück
		
		System.out.println(map.get(5)); // überschriebener Wert
		
		Set<Integer> s = map.keySet();
		for (Integer string : s) {
			String val = map.get(string);
			System.out.println("Key: " + string + " Wert: " + val);
		}
		
		/**
		 * Vereinfacht
		 */
		
		Map.Entry<Integer, String> entry;
		Set<Map.Entry<Integer, String>> set = map.entrySet();
		for (Map.Entry<Integer, String> entry2 : set) {
			System.out.println(entry2);
		}
	}
}
