package vormittag;

import java.util.*;

public class TextStatistics {
	private String text;
	public static TextStatistics ts;
	
	public TextStatistics(String s) {
		this.text = s;
		TextStatistics.ts = this;
	}
	public String getText() {
		return this.text;
	}

	public static void main(String[] args) {
		
		TextStatistics stat = TextStatistics.of("Heute ist Montag!");
		Collection<Character> chars = stat.getUniqueChars();
		System.out.println("Aufgabe 01");
		System.out.println(chars);
		Map<Character, Integer> countMap = stat.getCharCounts();
		System.out.println("\n" + countMap);
	}
	
	static TextStatistics of(String s) {		
		return new TextStatistics(s);
	}
	
	Collection<Character> getUniqueChars() {
		
		HashSet<Character> set = new HashSet<>();
		char[] ch = ts.getText().toCharArray();
		for (char c : ch) {
			set.add(c);
		}
		return set;
	}
	
	Map<Character, Integer> getCharCounts() {
		Map<Character, Integer> map = new TreeMap<>();
		String[] letters = ts.getText().toLowerCase().split("");
		Character[] chars = new Character[letters.length];
		for(int i = 0; i < letters.length; i++) {
			chars[i] = Character.valueOf(letters[i].charAt(0));
		}
		List<Character> letterList = Arrays.asList(Arrays.copyOfRange(chars, 1, chars.length));
		for (Character letter : new TreeSet<Character>(letterList)) {
			map.put(letter , Collections.frequency(letterList, letter));
			
		}
		return map;
	}
}
