package vormittag;

import java.io.File;
import java.io.FileFilter;
import java.util.*;

public class FileTypes {

	File files;
	
	public FileTypes(String s) {
		this.files = new File(s);
	}
	
	
	public static void main(String[] args) {
		
		FileTypes ft = new FileTypes("C:\\Windows");
		Collection<String> extColl = ft.getFileTypes();
		System.out.println(extColl);
		Map<String, Integer> files = ft.getFileCount();
		System.out.println(files);
	}
	
	
	Collection<String> getFileTypes() {
//		Collection<String> extensions = new TreeSet<>();
//		FileFilter filterFile = File::isFile;
//		File[] files = this.files.listFiles(filterFile);
//		for (File file : files) {
//			int i = file.getName().indexOf(".");
//			String ext = file.getName().substring(i, file.getName().length());
//			extensions.add(ext);
//		}
//		
//		
//		return extensions;
		return getFileCount().keySet();
	}
	
	Map<String, Integer> getFileCount() {
		Map<String, Integer> fileCount = new TreeMap<>();
		FileFilter filterFile = File::isFile;
		File[] files = this.files.listFiles(filterFile);
		String ext = "";
		for (File file1 : files) {
			int i = file1.getName().lastIndexOf(".");
			if(i <= 0 || i==file1.getName().length() -1) {
				ext = "<keine Erweiterung";
			} else {
				ext = file1.getName().substring(i, file1.getName().length());
				Integer count = fileCount.get(ext);
				if(count == null)
					count = 0;
				fileCount.put(ext, ++count);
			}
		}
		return fileCount;
	}
}
