package constructorTest;

public class VisibilityContructor {

	/**
	 * Kontruktoren in Java in einer normalen Klasse können nur die Sichtbarkeiten
	 * default und public haben! Sobald ein private Kontruktor hinzukommt gibt es eine Fehlermeldung.
	 * 
	 */
	
	public VisibilityContructor(String name) {
		/**
		 * Kontruktor mit Parameter und Public ohne Fehler.
		 * Kein Default-Konstruktor mehr vorhanden 
		 */
		this();
	}
	
	VisibilityContructor() {
		/**
		 * Kontruktor mit Default-Sichtbarkeit. Kein Fehler
		 */
		System.out.println("Konstruktor Test");
	}
	
//	private VisibilityContructor() {
		/**
		 * Ein Konstruktor mit private ist nicht möglich
		 */
//	}
	
//	protected VisibilityContructor() {
		/**
		 * Protected ist auch nicht Möglich
		 */
//	}
	
	public static void main(String[] args) {
		VisibilityContructor vc = new VisibilityContructor();
	}
}
