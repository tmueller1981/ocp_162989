package aufgabeLotto;

import java.util.Arrays;
import java.util.Random;

public class LottoSpiel {

	private int anzahlKugel;
	private int anzahlKugelGesamt;
	private int gewinn;
	public int[] ziehung;
	
	public LottoSpiel(int anzahlKugeln, int anzahlKugelGesamt) {
		this.setAnzahlKugeln(anzahlKugeln);
		this.setAnzahlGesamtKugeln(anzahlKugelGesamt);
	}
	
	public void setAnzahlKugeln(int anzahlKugel) {
		this.anzahlKugel = anzahlKugel;
	}
	
	public int getAnzahlKugel() {
		return this.anzahlKugel;
	}
	
	public void setAnzahlGesamtKugeln(int anzahlGesamtKugeln) {
		this.anzahlKugelGesamt = anzahlGesamtKugeln;
	}
	
	public int getAnzahlGesamtKugeln() {
		return this.anzahlKugelGesamt;
	}
	public void setGewinn(int gewinn) {
		this.gewinn = gewinn;
	}
	
	public int getGewinn() {
		return this.gewinn;
	}
	
	public int[] getZiehung() {
		return ziehung;
	}

	public void setZiehung(int[] ziehung) {
		this.ziehung = ziehung;
	}

	public int mehrZiehungen(int anzahl, int kugeln, int gesamt, LottoTip tip) {
		int kosten = 0;
		int gewinn = 0;
		LottoSpiel ls = new LottoSpiel(kugeln, gesamt);
		for (int i = 0; i < anzahl; i++) {
			ls.ziehen();
			ls.vergleichen(tip);
			kosten++;
		}
		if(kosten > gewinn) {
			gewinn = gewinn -kosten;
		} else {
			gewinn = gewinn - kosten;
		}			
		return gewinn; 
	}
	
	public String ziehen() {
		
		String ausgabe = "";
		
		int[] ziehung = new int[this.getAnzahlKugel()];
		for(int i = 0; i < getAnzahlKugel(); i++) {
			ziehung[i] = generateNumber();
		}
		Arrays.sort(ziehung);
		
		this.setZiehung(ziehung);
		findDoubles(ziehung, ziehung);
		for(int i : ziehung)
			ausgabe += " " + i;
		return ausgabe;
	}
	
	private int generateNumber() {
		int number = 0;
		Random r = new Random();
		number = r.nextInt(this.getAnzahlGesamtKugeln()-1+1) + 1;
		return number;
	}
	
	private void findDoubles(int[] a, int[] b) {
		for(int i = 0; i < a.length; i++) {
			for(int j = i+1; j < b.length; j++) {
				if(a[i] == b[j]) {
					a[i] = generateNumber();
					findDoubles(a, b);
				}
			}
		}
	}
	
	public String vergleichen(LottoTip tip) {
		Arrays.sort(tip.getZiehung());
		int count = 0;
		for (int i = 0; i < this.getZiehung().length; i++) { 
			for (int j = i + 1; j < tip.getZiehung().length; j++) { 
				if (String.valueOf(this.getZiehung()[i]).equals(String.valueOf(tip.getZiehung()[j]))) { 
					count++;
				}
			}
		}
		String ausgabe = "";
		switch(count) {
		default:
			ausgabe = "Sie haben 0 Richtige: 0 Euro gewonnen.";
			break;
		case 1:
			ausgabe = "Sie haben 1 Richtige: 10 Euro gewonnen.";
			this.setGewinn(10);
			break;
		case 2:
			ausgabe = "Sie haben 2 Richtige: 100 Euro gewonnen.";
			this.setGewinn(100);
			break;
		case 3:
			ausgabe = "Sie haben 3 Richtige: 1000 Euro gewonnen.";
			this.setGewinn(1000);
			break;
		case 4:
			ausgabe = "Sie haben 4 Richtige: 10000 Euro gewonnen.";
			this.setGewinn(10000);
			break;
		case 5:
			ausgabe = "Sie haben 5 Richtige: 100000 Euro gewonnen.";
			this.setGewinn(100000);
			break;
		case 6:
			ausgabe = "Sie haben 6 Richtige: 1000000 Euro gewonnen.";
			this.setGewinn(1000000);
			break;
		}
		
		return ausgabe;

			
	}
}
