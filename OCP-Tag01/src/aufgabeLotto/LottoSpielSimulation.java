package aufgabeLotto;

public class LottoSpielSimulation {

	public static void main(String[] args) {
		
		int anzahlKugel = 7;
		int anzahlKugelGesamt = 49;
		
		LottoSpiel lotto = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);
		System.out.println("Spiel 7 aus 49: \t\t" + lotto.ziehen());
		
		LottoTip tip = new LottoTip(anzahlKugel, anzahlKugelGesamt);
		System.out.println("Eigene Ziehung 7 aus 49: \t" + tip.abgeben());
		System.out.println("-----------------------------");
		System.out.println(lotto.vergleichen(tip));
		System.out.println("-----------------------------");
		System.out.println("Sie haben gewonnen: " + lotto.mehrZiehungen(100000, anzahlKugel, anzahlKugelGesamt, tip));
		
	}
}
