package aufgabeLotto;

import java.util.Arrays;
import java.util.Random;

public class LottoTip {

	private int anzahlKugel = 7;
	private int anzahlKugelGesamt = 49;
	private int[] ziehung;
	
	public LottoTip(int anzahlKugeln, int anzahlKugelGesamt) {
		this.setAnzahlKugel(anzahlKugeln);
		this.setAnzahlKugelGesamt(anzahlKugelGesamt);
	}

	public int getAnzahlKugel() {
		return anzahlKugel;
	}

	public void setAnzahlKugel(int anzahlKugel) {
		this.anzahlKugel = anzahlKugel;
	}

	public int getAnzahlKugelGesamt() {
		return anzahlKugelGesamt;
	}

	public void setAnzahlKugelGesamt(int anzahlKugelGesamt) {
		this.anzahlKugelGesamt = anzahlKugelGesamt;
	}
	
	public int[] getZiehung() {
		return this.ziehung;
	}
	
	public String abgeben() {
		
		String ausgabe = "";
		
		int[] ziehung = new int[this.getAnzahlKugel()];
		for(int i = 0; i < getAnzahlKugel(); i++) {
			ziehung[i] = generateNumber();
		}
		Arrays.sort(ziehung);
		
		findDoubles(ziehung, ziehung);
		this.ziehung = ziehung;
		for(int i : ziehung)
			ausgabe += " " + i;
		return ausgabe;
	}
	
	private int generateNumber() {
		int number = 0;
		Random r = new Random();
		number = r.nextInt(this.getAnzahlKugelGesamt()-1+1) + 1;
		return number;
	}
	
	private void findDoubles(int[] a, int[] b) {
		for(int i = 0; i < a.length; i++) {
			for(int j = i+1; j < b.length; j++) {
				if(a[i] == b[j]) {
					a[i] = generateNumber();
					findDoubles(a, b);
				}
			}
		}
	}
	
	
}
