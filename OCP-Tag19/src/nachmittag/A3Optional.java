package nachmittag;

import java.util.*;
import java.util.function.Supplier;

public class A3Optional {

	@SafeVarargs
	public static <A, T extends Collection<A>> T build( Supplier<T> s, A... values ) {
						
		T c = s.get();
		for (A a : values) {
			c.add(a);
		}
		return c;
	}
	
	public static void main(String[] args) {
		
		String[] a = {"a", "b", "c"};
		Supplier<String> sup;
		build(sup, );
	}
}
