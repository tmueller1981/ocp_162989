package nachmittag;
import java.util.*;

interface KannBehandelWerden {
	void setGesund(boolean gesund);
	boolean isGesund();
}

class Mensch implements KannBehandelWerden {
	boolean gesund;
	
	public void setGesund(boolean gesund) {
		this.gesund = gesund;
	}
	public boolean isGesund() {
		return this.gesund;
	}
}

abstract class Tier implements KannBehandelWerden {
	boolean gesund;
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " GEunsheit: " +gesund;
	}
}
class Zebra extends Tier {

	@Override
	public void setGesund(boolean gesund) {
		super.gesund = gesund;
		
	}

	@Override
	public boolean isGesund() {
		return super.gesund;
	}
	
}
class Affe extends Tier {

	@Override
	public void setGesund(boolean gesund) {
		super.gesund = gesund;
		
	}

	@Override
	public boolean isGesund() {
		return super.gesund;
	}
	
}

class Arzt extends Mensch {
	
	<T extends KannBehandelWerden>void behandeln(T t) {
		t.setGesund(true);
	}
}


public class Zoo {

	Tier t;
	static Collection<Tier> tiere = new ArrayList<Tier>();
	
	public Zoo(Tier t) {
		this.t = t;
		tiere.add(t);
	}
	
	static Collection<Tier> getTiere() {
		return tiere;
	}
	
	public static void main(String[] args) {
		
		Arzt affenarzt = new Arzt();
		Affe a1 = new Affe();
		Zebra z1 = new Zebra();
		
		System.out.println(a1);
		affenarzt.behandeln(a1);
		affenarzt.behandeln(z1);
		System.out.println(a1);
		
		System.out.println(getTiere().toString());
	}
}
