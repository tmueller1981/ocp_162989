package nachmittag;
import java.util.*;

public class GenerischeMethoden {

	static <T extends Comparable<T>> T getMax(T t1, T t2) {
		if(t1.compareTo(t2) > 0)
			return t1;
		return t2;
	}
	
	
	public static void main(String[] args) {
		
		String s = getMax("abc", "def"); // getMax liefert "def"
		System.out.println(s);
		
		Integer i = getMax(14, 12); // getMax liefert Integer mit 14
		System.out.println(i);

		Date d = getMax(new Date(), new Date(0)); // getMax liefert das sp�tere Datum
		System.out.println(d);
		
		//getMax("hallo", 22); // hier soll ein Compilerfehler entstehen
	}
	
}
