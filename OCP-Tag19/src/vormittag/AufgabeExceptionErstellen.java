package vormittag;

import java.io.IOException;
import java.util.function.Supplier;

public class AufgabeExceptionErstellen {

	static <T extends Throwable> void checkAndThrow(boolean test, Supplier<T> e) throws T {
		if(!test) {
			T ex = e.get();
			throw ex;
		}
	}
	
	
	public static void main(String[] args) {		
		
		checkAndThrow(args.length == 1, () -> new IllegalArgumentException("Erwarte ein Argument f�r main"));
		
		
		
		try {
			boolean input = args.length == 0;
			checkAndThrow(input, () -> new IOException("IO stimmt nicht"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
