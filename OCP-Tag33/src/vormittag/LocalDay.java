package vormittag;

import java.util.Locale;
import java.util.stream.Stream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.*;

public class LocalDay {

	// Stream<Locale> langS = Arrays.stream(lang) ;
	//
	// Map<Locale, LocalDate> day = null;

	public static void main(String[] args) {
		Locale[] lang = Locale.getAvailableLocales();

		Arrays.sort(lang, Comparator.comparing(Locale::getDisplayLanguage));

		System.out.printf("| %10s | %5s | %15s | %15s | %n", "Sprache", "Land",
				"Tag", "Monat");
		for (int i = 0; i < lang.length; i++) {
			System.out.printf(lang[i], "| %10s | %5s | %15s | %15s | %n",
					lang[i].getLanguage(), lang[i].getCountry(), LocalDate.now()
							.getDayOfWeek()
							.getDisplayName(TextStyle.FULL, lang[i]),
					LocalDate.now()
							.getMonth()
							.getDisplayName(TextStyle.FULL, lang[i]));
		}

	}

}
