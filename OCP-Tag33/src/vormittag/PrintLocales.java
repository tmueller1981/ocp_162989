package vormittag;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

public class PrintLocales {

	public static void main(String[] args) {

		Locale[] languages = Locale.getAvailableLocales();
		printTable(languages);
	}

	static void printTable(Locale[] locales) {

		Arrays.sort(locales, Comparator.comparing(Locale::getCountry)
				.thenComparing(Comparator.comparing(Locale::getDisplayLanguage))
				.reversed());

		System.out.printf("| %5s | %35s | %20s | %5s | %n%n", "Nr.", "Land",
				"Sprache", "Code");
		for (int i = 0; i < locales.length; i++) {
			System.out.printf("| %5s | %35s | %20s | %5s | %n", i + 1,
					locales[i].getDisplayCountry(),
					locales[i].getDisplayLanguage(), locales[i].getCountry());
		}

	}
}
