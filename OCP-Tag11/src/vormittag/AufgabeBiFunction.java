package vormittag;

import java.util.function.BiFunction;

class Auto {
	@Override
	public String toString() {
		return "Auto (" + hashCode() + ")";
	}
	
	public Besitzer buildBesitzer(Integer id) {
		return new Besitzer(this, id);
	}
}
class Besitzer {
	private Auto a;
	private int id;
	public Besitzer(Auto a, Integer id) {
		this.a = a;
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Besitzer vom " + a + ". ID: " + id;
	}
}

class BesitzerBuilder {
	public static Besitzer build(Auto a, Integer id) {
		return new Besitzer(a, id);
	}
	
	public Besitzer create(Auto a, Integer id)  {
		return new Besitzer(a, id);
	}
}

public class AufgabeBiFunction {

	public static void main(String[] args) {
		
		a1();
		a2();
		a3();
		a4();
		a5();
		a6();
	}
	
	public static void a6() {
		System.out.println("*** Aufgabe 6");
		BiFunction<Auto, Integer, Besitzer> f6 = Auto::buildBesitzer;
		System.out.println("b6: " + f6.apply(new Auto(), 17));
	}
	
	public static void a5() {
		System.out.println("*** Aufgabe 5");
		
		BesitzerBuilder particularObject = new BesitzerBuilder();
		
		BiFunction<Auto, Integer, Besitzer> f5 = particularObject::create;
		System.out.println("b5: " + f5.apply(new Auto(), 16));
	}
	
	public static void a4() {
		System.out.println("*** Aufgabe 4");
		
		BiFunction<Auto, Integer, Besitzer> f4 = Besitzer::new;
		Besitzer b4 = f4.apply(new Auto(), 15);
		System.out.println("b4: " + b4);
	}
	
	public static void a3() {
		System.out.println("*** Aufgabe 3");
		BiFunction<Auto, Integer, Besitzer> f3 = BesitzerBuilder::build;
		
		Besitzer b3 = f3.apply(new Auto(), 14);
		System.out.println("b3: " + b3);
	}
	
	public static void a2() {
		System.out.println("*** Aufgabe 2");
		BiFunction<Auto, Integer, Besitzer> f2 = (auto, id) -> new Besitzer(auto, id);
		Besitzer b2 = f2.apply(new Auto(), 13);
		System.out.println("b2: " +b2);
	}
	
	public static void a1() {
		System.out.println("*** Aufgabe 1");
		BiFunction<Auto, Integer, Besitzer> f1 = new BiFunction<Auto, Integer, Besitzer>() {
			public Besitzer apply(Auto a, Integer i) {
				return new Besitzer(a, i);
			}
		};
		Besitzer b1 = f1.apply(new Auto(), 12);
		System.out.println("b1: " +b1);
	}	
}
