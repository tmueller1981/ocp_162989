package vormittag;

import java.time.LocalDate;
import java.util.Comparator;

public class AufgabeComparator {

	public static void main(String[] args) {
		
		LocalDate date1 = LocalDate.now();
		LocalDate date2 = LocalDate.of(2019, 06, 20);
		
		Comparator<LocalDate> date = (d, d2) -> d.getDayOfMonth() - d2.getDayOfMonth();
		
		int i = date.compare(date1, date2);

		if(i > 0)
			System.out.println("Date A ist nach Date B");
		else
			System.out.println("Date B ist nach Date A");
		
	}
}
