package nachmittag;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class AufgabeVerzeichnisse {

	public static void main(String[] args) {

		// createDirs("a\\b\\c\\d");
		// deleteDirs("a\\b\\c\\d");
		// printSubdirs(Paths.get("c:\\"));
		// printFiles(Paths.get("c:\\"));

	}

	static void printSubdirs(Path dir) {

		// if (Files.isDirectory(dir))
		try {

			Stream<Path> files = Files.list(dir);
			files.filter(p -> Files.isDirectory(p))
					.forEach(System.out::println);;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void printFiles(Path dir) {
		try {

			Stream<Path> files = Files.list(dir);
			files.filter(p -> Files.isRegularFile(p))
					.forEach(System.out::println);;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void createDirs(String dirs) {

		try {
			Files.createDirectories(Paths.get(dirs));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void deleteDirs(String dirs) {

		try {
			Path p = Paths.get(dirs);

			while (p != null) {
				Files.delete(p);
				p = p.getParent();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
