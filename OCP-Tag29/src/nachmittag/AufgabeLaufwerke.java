package nachmittag;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import java.util.ArrayList;
import java.util.Comparator;

public class AufgabeLaufwerke {

	public static void main(String[] args) {

		print();
	}

	static void print() {

		System.out.printf("%-30s %13s %15s%n", "Laufwerk", "usable(MB)",
				"total(MB)");

		ArrayList<FileStore> fs = new ArrayList<>();

		try {
			for (FileStore store : FileSystems.getDefault()
					.getFileStores()) {

				fs.add(store);

				System.out.format("%-30s %13d %15d%n", store,
						(store.getUsableSpace() / 1024 / 1024),
						(store.getTotalSpace() / 1024 / 1024));

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		fs.sort(Comparator.comparing(t -> {
			try {
				return t.getUsableSpace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}));

		System.out.println("\n*** Aufgabe 01");
		try {
			for (FileStore s : fs) {
				System.out.format("%-30s %13d %15d%n", s,
						(s.getUsableSpace() / 1024 / 1024),
						(s.getTotalSpace() / 1024 / 1024));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream("Laufwerke.txt"))) {
			// oos.writeObject(fs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
