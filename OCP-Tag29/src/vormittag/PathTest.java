package vormittag;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTest {

	public static void main(String[] args) {

		Path p1 = Paths.get("c:\\");
		Path p2 = Paths.get("haloo");
		System.out.println(p1.resolve(p2));
		System.out.println(p2.resolve(p1));

	}
}
