package vormittag;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AufgabeFilesSimple {

	public static void main(String[] args) {

		erzeugeTestumgebung();

		entferneTestumgebung();
	}

	static void erzeugeTestumgebung() {

		Path p = Paths.get("autos");
		try {
			Files.createDirectory(p);

			Files.createDirectories(p.resolve("pkws"));
			Files.createDirectories(p.resolve("lkws"));

			Files.createFile(p.resolve("pkws/vw.txt"));
			Files.createFile(p.resolve("lkws/man.txt"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void entferneTestumgebung() {

		try {
			Files.delete(Paths.get("autos/pkws/vw.txt"));
			Files.delete(Paths.get("autos/lkws/man.txt"));
			Files.delete(Paths.get("autos/pkws"));
			Files.delete(Paths.get("autos/lkws"));
			Files.delete(Paths.get("autos"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
