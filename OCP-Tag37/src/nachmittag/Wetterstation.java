package nachmittag;

import java.util.Random;

class Temperatur implements Runnable {

	Random wert = new Random();
	int temp = 0;
	int akt = 0;

	@Override
	public void run() {
		while (true) {
			akt = wert.nextInt(5 + 5) - 5;
			temp = temp + akt;
			System.out.println("Aktuelle Temperatur: " + (temp) + " Celsius");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}

public class Wetterstation {

	public static void main(String[] args) {

		Temperatur temp = new Temperatur();
		Thread t = new Thread(temp);
		t.start();
	}
}
