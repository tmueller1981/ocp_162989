package nachmittag;

public class MyLogger {
	private StringBuilder sb = new StringBuilder();

	synchronized public void addMessage(String caller, String message) {
		sb.append(caller)
				.append(" - ")
				.append(message)
				.append("\n");
	}

	public String getLog() {
		return sb.toString();
	}

	public static void main(String[] args) {

		MyLogger log = new MyLogger();

		Runnable target = () -> {
			for (int i = 0; i < 10; i++) {

				log.addMessage("Logger", "Message");

			}

		};
		Runnable target2 = () -> {
			for (int i = 0; i < 10; i++) {

				log.addMessage("Logger2", "Message2");

			}

		};

		Thread th1 = new Thread(target);
		Thread th2 = new Thread(target2);
		th1.start();
		th2.start();

		System.out.println("*** A1");
		System.out.println(log.getLog());

		System.out.println("\n*** A2");
		System.out.println(log.getLog());
	}

}
