package vormittag;

class OwnPrinter extends Thread {

	char[][] zeichen;
	char c;
	int col, row;

	public OwnPrinter(char c, int col, int row) {
		this.c = c;
		this.col = col;
		this.row = row;
	}

	@Override
	public void run() {
		zeichen = new char[row][col];

		synchronized (this) {
			for (int i = 0; i < zeichen.length; i++) {
				for (int y = 0; y < zeichen[i].length; y++) {
					zeichen[i][y] = c;
				}
			}
		}
		for (char[] ds : zeichen) {
			System.out.println(ds);
		}
	}

}

public class AufgabePrinter {

	public static void main(String[] args) {

		OwnPrinter p1 = new OwnPrinter('a', 10, 200);
		p1.start();

		OwnPrinter p2 = new OwnPrinter('*', 15, 400);
		p2.start();

	}
}
