package vormittag;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AufgabeThreadsInteract {

	volatile static int countA3;
	volatile static int countA4;

	public static void main(String[] args) throws InterruptedException {

		// a1();
		// a2();
		// a3();
		a4();

	}

	static void a4() {

		int anzahlWiederholungen = 1_000_000;
		Object monitor = "";

		Runnable inc = () -> {
			for (int i = 0; i < anzahlWiederholungen; i++) {
				synchronized (monitor) {
					countA4++;
				}
			}
		};

		Runnable dec = () -> {
			for (int i = 0; i < anzahlWiederholungen; i++) {
				synchronized (monitor) {
					countA4--;
				}

			}
		};

		Thread thInc = new Thread(inc);
		Thread thDec = new Thread(dec);
		thInc.start();
		thDec.start();

		try {
			thInc.join();
			thDec.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("count: " + countA4);
	}

	static void a3() {

		Thread th = new Thread(() -> {
			for (int i = 0; i < 1_000_000; i++) {
				countA3++;
			}
		});
		th.start();
		try {
			th.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("count: " + countA3);
	}

	static void a2() {
		class RandomValues implements Runnable {
			int numberOfValues;
			List<Integer> values;

			public RandomValues(int number) {
				this.numberOfValues = number;
			}
			@Override
			public void run() {
				Random r = new Random();

				values = Stream.generate(() -> 1)
						.limit(numberOfValues)
						.collect(Collectors.toList());
			}
			public List<Integer> getValues() {
				return values;
			}

		}
		RandomValues valuesA = new RandomValues(30);
		RandomValues valuesB = new RandomValues(30);

		Thread threadA = new Thread(valuesA);
		Thread threadB = new Thread(valuesB);
		threadA.start();
		threadB.start();

		try {
			threadA.join();
			threadB.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// System.out.println(valuesA.getValues());
		// System.out.println(valuesB.getValues());

		int summe = Stream.concat(valuesA.getValues()
				.stream(),
				valuesB.getValues()
						.stream())
				.mapToInt(x -> x)
				.sum();
		System.out.println("sum: " + summe);
	}

	static void a1() throws InterruptedException {
		Thread t = Thread.currentThread();

		Runnable target = () -> {
			while (true) {
				System.out.println("laufe...");

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println(t + "wird beendet");
					break;
				}

			}
		};

		Thread th = new Thread(target);
		th.start();

		Thread.sleep(5000);
		System.out.println("main setzt den th-thread auf interrupted");
		th.interrupt();
	}
}
