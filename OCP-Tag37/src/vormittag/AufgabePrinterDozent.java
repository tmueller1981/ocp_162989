package vormittag;

class Printer extends Thread {

	private char ch;
	private int charsInRow, rows;

	public Printer(char ch, int charsInRow, int rows) {
		this.ch = ch;
		this.charsInRow = charsInRow;
		this.rows = rows;
	}

	@Override
	public void run() {

		// String row = "";
		// for (int i = 0; i < charsInRow; i++) {
		// row += ch;
		// }
		// for (int i = 0; i < rows; i++) {
		// System.out.println(row);
		// }

		for (int i = 0; i < rows; i++) {
			synchronized (Printer.class) {
				for (int j = 0; j < charsInRow; j++) {
					System.out.print(ch);
				}
				System.out.println();
			}
		}
	}
}

public class AufgabePrinterDozent {

	public static void main(String[] args) {

		Printer p1 = new Printer('a', 10, 20);
		p1.start();

		Printer p2 = new Printer('*', 15, 40);
		p2.start();

	}
}
