package vormittag;

import java.time.ZoneId;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AufgabeZonedId {

	public static void main(String[] args) {

		// meineLoesung();
		dozentenLoesung();

	}

	static void dozentenLoesung() {

		Function<String, String> classifier = zoneIdName -> {

			int indexSlash = zoneIdName.indexOf("/");
			if (indexSlash == -1) {
				return "Special";
			}
			return zoneIdName.substring(0, indexSlash);

		};
		Collector<String, ?, Map<String, List<String>>> c1 = Collectors
				.groupingBy(classifier);

		Map<String, List<String>> mapGroups = ZoneId.getAvailableZoneIds()
				.stream()
				.sorted()
				.collect(c1);

		Set<String> groupName = mapGroups.keySet();
		for (String group : groupName) {
			System.out.println("Gruppe: '" + group + "' ");
			List<String> zoneNames = mapGroups.get(group);
			for (String zidName : zoneNames) {
				System.out.println(zidName);
			}
		}
	}

	static void meineLoesung() {
		Function<String, String> func = s -> s.substring(0,
				s.contains("/") ? s.indexOf("/") : s.length());

		Collector<String, ?, Map<String, List<String>>> collector = Collectors
				.groupingBy(func);

		Collection<List<String>> map = ZoneId.getAvailableZoneIds()
				.stream()
				.sorted(Comparator.naturalOrder())
				.collect(collector)
				.values();
		map.forEach(System.out::println);
	}
}
