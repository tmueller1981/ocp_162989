package vormittag;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class AufgabeZeitspanne {

	public static void main(String[] args) {

		// a1();
		// a2();
		a3();
	}

	static void a3() {
		System.out.println("*** A3");

		int stunde = 1;
		ZoneId zone = ZoneId.of("Europe/Berlin");
		ZonedDateTime z1 = ZonedDateTime.of(2018, 3, 25, stunde, 0, 0, 0, zone);

		stunde = 4;
		ZonedDateTime z2 = ZonedDateTime.of(2018, 3, 25, stunde, 0, 0, 0, zone);

		Duration d = Duration.between(z1, z2);
		System.out.println("dureation: " + d);
		System.out.println("stunden: " + d.toHours());
	}

	static void a2() {

		System.out.println("*** A2");
		int startHour = 8;
		ZoneId startZone = ZoneId.of("UTC+1");
		ZonedDateTime start = ZonedDateTime.of(2019, 7, 25, startHour, 0, 0, 0,
				startZone);

		ZonedDateTime endUTC1 = start.plusHours(3);
		System.out.println("Ankunft ()UTC1: " + endUTC1);
		ZoneId endZone = ZoneId.of("UTC+2");
		ZonedDateTime endUTC2 = endUTC1.withZoneSameInstant(endZone);

		System.out.println("Ankunft (UTC2): " + endUTC2);
	}

	static void a1() {
		System.out.println("*** A1");

		/**
		 * Start 12:00 Uhr in der Zeitzone UTC+1 England: 11:00 Uhr
		 */
		LocalDateTime startTime = LocalDateTime.now()
				.withHour(12)
				.withMinute(0);
		ZonedDateTime start = ZonedDateTime.of(startTime, ZoneId.of("UTC+1"));
		System.out.println("Start: " + start);

		/**
		 * Ende 16:00 Uhr in der Zeitzone UTC+2 England: 14:00 Uhr
		 */
		LocalDateTime endTime = startTime.withHour(16);
		ZonedDateTime ende = ZonedDateTime.of(endTime, ZoneId.of("UTC+2"));
		System.out.println("Ende: " + ende);

		long dauer = ChronoUnit.HOURS.between(startTime, endTime);
		System.out.println("Dauer: " + dauer + " Stunden");

	}
}
