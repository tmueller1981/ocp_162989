package vormittag;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

public class AufgabeDateTime {

	public static void main(String[] args) {

		// a1();
		printCalendar(4, 2010);
	}

	public static void a1() {
		ZoneId.getAvailableZoneIds()
				.stream()
				.sorted(Comparator.naturalOrder())
				.forEach(System.out::println);
	}

	static void printCalendar(int month, int year) {
		YearMonth ym = YearMonth.of(year, month);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd | EEEE ");
		String monthName = ym.format(DateTimeFormatter.ofPattern("MMMM"));
		System.out.println(monthName + " " + year);
		System.out.println("---------------------");
		for (int day = 1; day <= ym.lengthOfMonth(); day++) {
			LocalDate date = LocalDate.of(year, month, day);
			String dayname = date.format(formatter);
			System.out.println(dayname);
		}
	}

}
