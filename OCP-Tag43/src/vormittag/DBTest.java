package vormittag;

import java.sql.*;
import java.util.Enumeration;

public class DBTest {

	public static void main(String[] args) {

		String url = "jdbc:ucanaccess://E:\\01_Dokumente\\Java Entwicklung\\OCP_162989\\WS_TN17\\ocp-online\\OCP-Tag43\\Kontaktverwaltung.accdb";

		// Verbindung aufbauen
		// seit JDBC 4.x

		// fr�her JDBC 3.x
		// class.forname("DB-Treiber Name");

		// try {
		// Connection conn = DriverManager.getConnection(url);
		// System.out.println("Verbindung erfolgreich");
		// } catch (SQLException e) {
		// e.printStackTrace();
		// }
		//
		// for (Enumeration<Driver> e = DriverManager.getDrivers(); e
		// .hasMoreElements();)
		// System.out.println(e.nextElement());

		// Seit JAVA 7 try with Ressources

		try (Connection conn = DriverManager.getConnection(url);
				Statement stmt = conn.createStatement();) {
			System.out.println("Verbindung erfolgreich!");
			String sql = "SELECT * FROM Mitarbeiter";
			sql = "UPDATE Mitarbeiter SET Name = 'Braun' WHERE PersonalNr = 6";
			// sql = "UPDATE Mitarbeiter SET [Tel intern] = [Tel intern] +
			// 75000";
			int count = stmt.executeUpdate(sql);
			System.out.println("count: " + count);

			// sql = "SELECT * FROM Mitarbeiter WHERE Ort = 'Frankfurt'";
			sql = "SELECT * FROM Mitarbeiter";
			ResultSet rs = stmt.executeQuery(sql);

			// System.out.println(rs.getString(1)); Gibt eine Exception
			while (rs.next()) {
				if (rs.getString("Ort")
						.equals("Frankfurt")) {
					System.out.printf("PNr. %10s | %15s , %15s | %15s %n",
							rs.getString(1), rs.getString("Name"),
							rs.getString("Vorname"), rs.getString("Ort"));
				}
			}

			// Hinzuf�gen einer neuen Kontaktart
			String kontaktart = "Vertriebsoffensive";
			sql = "INSERT INTO Kontaktarten VALUES (1,  '" + kontaktart + "')";
			// sql = "INSERT INTO Kontaktarten(kontaktart) VALUES ( '" +
			// kontaktart + "')"; geht auch
			stmt.execute(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
