package aufgaben;

import java.util.Random;

public class IntMatrix {
	int[][] arr;
	
	public IntMatrix(int col, int row) {
		arr = new int[col][row];
		
	}
	
	public IntMatrix(int col, int row, int value) {
		
		arr = new int[col][row];
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				arr[i][j] = value;
			}
		}
	}
	

	
	public static void main(String[] args) {
		
		IntMatrix m1 = new IntMatrix(2, 3);
		IntMatrix m2 = new IntMatrix(5, 3, 100);
		
		System.out.println("*** Aufgabe 01");
		System.out.println(m1);
		System.out.println("*** Aufgabe 02");
		System.out.println(m1.get(1, 2));
		System.out.println("*** Aufgabe 03");
		IntMatrix m3 = IntMatrix.getRandomMatrix(4,6,200);
		System.out.println(m3);
		IntMatrix m4 = m1;
		System.out.println("*** Aufgabe 04");
		System.out.println(m1.equals(m4));
	}
	
	public static IntMatrix getRandomMatrix(int row, int col, int random) {
		IntMatrix m = new IntMatrix(row, col);
		int[][] tmp = new int[col][row];
		Random rnd = new Random();
		for(int i = 0; i < tmp.length; i++) {
			for(int j = 0; j < tmp[i].length; j++) {
				tmp[i][j] = rnd.nextInt(random);
			}
		}
		m.arr = tmp;
		return m;
	}
	
	public int get(int col, int row) {
		int res = 0;
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				res = arr[i][j];
			}
		}		
		
		return res;
	}
	
	@Override
	public String toString() {
		String res = ""; 
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				res += String.valueOf(arr[i][j]) + " ";
			}
			res += "\n";
		}
		
		return res;
	}
}
