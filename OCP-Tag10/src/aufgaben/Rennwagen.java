package aufgaben;

public class Rennwagen {
	private String marke;
	private Fahrer f;
	private Motor m;
	
	public Rennwagen(String marke) {
		this.marke = marke;
	}
	
	public void setFahrer(Fahrer f) {
		this.f = f;
	}
	
	public Motor getMotor() {
		this.m = new Motor("Type1");
		return this.m;
	}
	
	public String getMarke() {
		return this.marke;
	}
	
	class Motor {
		private String typ;
		
		public Motor(String typ) {
			this.typ = typ;
		}
		
		@Override
		public String toString() {
			return "Motor " + this.typ + " aus dem Rennwagen " + Rennwagen.this.getMarke();
		}
	}

	public static class Fahrer {
		private String vorname;
		private String nachname;

		
		public Fahrer(String vorname, String nachname) {
			this.vorname = vorname;
			this.nachname = nachname;
		}
		
		@Override
		public String toString() {
			return this.vorname + " " + this.nachname;
		}
		

	}
	
	@Override
	public String toString() {
		return "Rennwagen " + this.marke +". Fahrer: " + this.f;
	}
}
