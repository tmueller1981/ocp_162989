package aufgaben;

public class Gebaeude {

	private String strasse;
	private int hnr;
	private int stockwerke;
	private int rStockwerk;
	private Stockwerk[] sw;
	private Raum[] r;
	
	
	public Gebaeude(String strasse, int hnr, int stockwerke, int rStockwerk) {
		this.strasse = strasse;
		this.hnr = hnr;
		this.stockwerke = stockwerke;
		this.rStockwerk = rStockwerk;
		this.sw = new Stockwerk[stockwerke];
		for(int i = 0; i < stockwerke; i++) {
			this.sw[i] = new Stockwerk();
		}
		this.r = new Raum[rStockwerk];
		for(int i = 0; i < rStockwerk; i++) {
			this.r[i] = new Raum();
		}
	}
	
	class Stockwerk {
		
	}
	
	class Raum {
		
	}
	
	
	public static void main(String[] args) {
		
		Gebaeude g1 = new Gebaeude("Hauptstraße", 45, 3, 10);
		System.out.println(g1.getRaum(0, 2));
	}
	
	public Stockwerk getStockwerk(int stockwerkNr) {
		return Gebaeude.this.sw[stockwerkNr];
	}
	
	public String getRaum(int stockwerkNr, int raumNr) {
		return "Raum " + String.valueOf(stockwerkNr) +"."+ String.valueOf(raumNr)+ " / " + this.strasse + " " + this.hnr;
	}
}
