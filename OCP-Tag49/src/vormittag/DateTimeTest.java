package vormittag;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateTimeTest {

	public static void main(String[] args) {

		a1();
		System.out.println();
		a2();
		System.out.println();
		a3();
		System.out.println();
		a4();
	}

	static void a1() {
		LocalTime time = LocalTime.of(8, 02);
		LocalTime dif1 = LocalTime.of(9, 32);

		LocalTime dif2 = LocalTime.of(9, 52);
		LocalTime dif3 = LocalTime.of(11, 19);

		LocalTime dif4 = LocalTime.of(11, 40);
		LocalTime dif5 = LocalTime.of(13, 01);
		Duration d = Duration.between(time, dif1);
		Duration d2 = Duration.between(dif2, dif3);
		Duration d3 = Duration.between(dif4, dif5);
		Duration end = Duration.from(d)
				.plus(d2)
				.plus(d3);

		long min = ChronoUnit.MINUTES.between(time, dif1)
				+ ChronoUnit.MINUTES.between(dif2, dif3)
				+ ChronoUnit.MINUTES.between(dif4, dif5);

		System.out.println("Wir hatten " + min + " Minuten Java");
		System.out.println("Wir hatten " + end.toMinutes() + " Minuten Java");

	}

	static void a2() {
		LocalDate birth = LocalDate.of(1981, 5, 30);
		LocalDate act = LocalDate.now();

		System.out.println("Du bist " + ChronoUnit.DAYS.between(birth, act)
				+ " Tage alt.");
	}

	static void a3() {
		LocalDate start = LocalDate.of(1900, 1, 1);
		LocalDate end = LocalDate.of(1999, 12, 31);
		Period p = Period.between(start, end);

		long days = ChronoUnit.DAYS.between(start, end);
		System.out.println("Das 20 Jahrhundert hatte " + days + " Tage");
	}

	static void a4() {
		ZonedDateTime start = ZonedDateTime.of(
				LocalDateTime.of(2019, 8, 21, 19, 50),
				ZoneId.of("Europe/Berlin"));

		Duration d1 = Duration.ofHours(6)
				.plusMinutes(20);

		start = start.plus(d1)
				.withZoneSameLocal(ZoneId.of("Asia/Dubai"));

		ZonedDateTime dub = ZonedDateTime.of(start.toLocalDateTime(),
				ZoneId.of("Asia/Dubai"));
		long hours = ChronoUnit.HOURS.between(start, dub);

		dub = dub.plusHours(2);
		dub = dub.plusMinutes(45);

		Duration d2 = Duration.ofHours(8)
				.plusMinutes(10);
		dub = dub.plus(d2);
		ZonedDateTime syd = ZonedDateTime.of(dub.toLocalDateTime(),
				ZoneId.of("Australia/Sydney"));

		Duration d3 = d1.plus(d2);
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		System.out.println("Wir kommen an: " + syd.format(formatter)
				+ " wir waren " + d3.toHours() + " Stunden und "
				+ d3.toMinutes() + " Minuten unterwegs.");
	}
}
