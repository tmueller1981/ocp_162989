package vormittag;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;
import java.io.*;
import java.nio.file.Files;

public class AufgabePfade {

	public static void main(String[] args) {

		String rootName = "c:\\Windows";
		List<File> dirs = list(rootName, File::isDirectory);
		System.out.println("Directories: " + dirs.size());

		dirs = list(rootName, File::isFile);
		System.out.println("Files: " + dirs.size());

		System.out.println("byte: " + getFileSize(dirs) + ", MB: "
				+ (getFileSize(dirs) / 1024. / 1024.));

		getTotalSize(dirs);
	}

	static void getTotalSize(List<File> files) {

		Function<File, String> f = (f1) -> f1.getPath();
		Function<String, List<File>> f2 = (f3) -> list(f3, File::isFile);
		files.stream()
				.map(f)
				.map(f2)
				.forEach(AufgabePfade::getFileSize);
	}

	static long getFileSize(List<File> files) {

		return files.stream()
				.mapToLong(File::length)
				.reduce(0, (a, b) -> a + b);

	}

	static List<File> list(String rootName, FileFilter filter) {

		File f = new File(rootName);
		File[] list = f.listFiles(filter);
		if (list == null) {
			return Collections.emptyList();
		}
		return Arrays.asList(list);
	}
}
