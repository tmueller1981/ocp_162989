package nachmittag;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class Auto {
	private String hersteller, modell;
	public Auto(String hersteller, String modell) {
		this.hersteller = hersteller;
		this.modell = modell;
	}
	public String getHersteller() {
		return hersteller;
	}
	public String getModell() {
		return modell;
	}

	@Override
	public String toString() {
		return hersteller + "/" + modell;
	}

	public static void main(String[] args) {

		List<Auto> autos = Arrays.asList(new Auto("VW", "Golf"),
				new Auto("VW", "Polo"), new Auto("Opel", "Corsa"),
				new Auto("Opel", "Astra"));

		System.out.println("*** Aufgabe 01");
		a1(autos);
		System.out.println("\n*** Aufgabe 02");
		a2(autos);
		System.out.println("\n*** Aufgabe 03");
		a3(autos);
		System.out.println("\n*** Aufgabe 04");
		a4(autos);
		System.out.println("\n*** Aufgabe 05");
		a5(autos);
	}

	static void a5(List<Auto> liste) {

		Function<Auto, String> classF = a -> a.getHersteller();
		Predicate<Auto> predicate = p -> p.getModell()
				.contains("o");
		Collector<Auto, ?, Map<Boolean, List<Auto>>> collector = Collectors
				.partitioningBy(predicate);

		Map<Boolean, List<Auto>> map = liste.stream()
				.collect(collector);
		System.out.println(map);
	}

	static void a4(List<Auto> liste) {

		Function<Auto, String> classF = a -> a.getHersteller();
		Collector<Auto, ?, Map<String, List<Auto>>> collector = Collectors
				.groupingBy(classF, TreeMap::new, Collectors.toList());

		Map<String, List<Auto>> map = liste.stream()
				.collect(collector);
		System.out.println(map);
	}

	static void a3(List<Auto> liste) {

		Function<Auto, String> classF = a -> a.getHersteller();
		Collector<Auto, ?, List<String>> downstream = Collectors
				.mapping(Auto::getModell, Collectors.toList());
		Collector<Auto, ?, Map<String, List<String>>> collector = Collectors
				.groupingBy(classF, downstream);

		Map<String, List<String>> map = liste.stream()
				.collect(collector);
		System.out.println(map);
	}

	static void a2(List<Auto> liste) {

		Function<Auto, String> classF = a -> a.getHersteller();
		Collector<Auto, ?, Map<String, List<Auto>>> collector = Collectors
				.groupingBy(classF);

		Map<String, List<Auto>> map = liste.stream()
				.collect(collector);
		System.out.println(map);
	}

	static void a1(List<Auto> liste) {

		Collector<String, ?, Set<String>> downstream = Collectors.toSet();

		Function<Auto, String> mapper = a -> a.getHersteller();
		Collector<Auto, ?, Set<String>> collector = Collectors.mapping(mapper,
				downstream);

		Set<String> set = liste.stream()
				.collect(collector);
		System.out.println(set); // m�gliche Ausgabe: [VW, Opel]

	}
}
