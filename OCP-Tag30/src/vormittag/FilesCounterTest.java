package vormittag;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

class FilesCounter {
	Path path;

	public FilesCounter(String pfad) {
		this.path = Paths.get(pfad);
	}

	int count(String ext) {

		BiPredicate<Path, BasicFileAttributes> matcher = (p, attr) -> attr
				.isRegularFile();
		try {
			Stream<Path> fileExt = Files.find(this.path, 1, matcher);
			return (int) fileExt.map((p) -> p.getFileName())
					.map((f) -> f.toString())
					.filter((f) -> f.contains("." + ext))
					.count();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}

	int countDeepDirectoryStream(String ext) {

		int count = 0;
		DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
			@Override
			public boolean accept(Path file) throws IOException {
				return file.getFileName()
						.toString()
						.contains("." + ext);
			}
		};

		try {
			DirectoryStream<Path> fileExt = Files.newDirectoryStream(path,
					filter);

			for (Path p : fileExt) {
				if (Files.isDirectory(p))
					count += countDeepDirectoryStream(p.toString());
				count++;
			}
			return count;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return count;
	}

	int countDeepWalk(String ext) {

		try {
			Stream<Path> fileExt = Files.walk(this.path, Integer.MAX_VALUE);
			return (int) fileExt.map((p) -> p.getFileName())
					.map((f) -> f.toString())
					.filter((f) -> f.contains("." + ext))
					.count();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	int countDeepWalkTree(String ext) {

		AtomicInteger count = new AtomicInteger();

		FileVisitor<Path> visitor = new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir,
					BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file,
					BasicFileAttributes attrs) throws IOException {

				if (file.toString()
						.contains("." + ext)) {
					count.incrementAndGet();
				}

				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc)
					throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc)
					throws IOException {
				return FileVisitResult.CONTINUE;
			}
		};

		try {
			Files.walkFileTree(this.path, visitor);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return count.get();
	}

}

public class FilesCounterTest {

	public static void main(String[] args) {

		FilesCounter fc = new FilesCounter("E:\\Programme\\PDF24");

		System.out.println("*** Aufgabe A2");
		long startTime = System.currentTimeMillis();
		int anzahl = fc.count("txt");
		long endTime = System.currentTimeMillis();
		System.out.println("Es gibt " + anzahl + " Textdateien.");
		System.out.println("Der Vorgang dauerte: "
				+ ((endTime - startTime) / 1000.) + " Sek.");

		System.out.println("\n*** Aufgabe A3 newDirectoryStream");
		startTime = System.currentTimeMillis();
		anzahl = fc.countDeepDirectoryStream("txt");
		endTime = System.currentTimeMillis();
		System.out.println("Es gibt " + anzahl + " Textdateien.");
		System.out.println("Der Vorgang dauerte: "
				+ ((endTime - startTime) / 1000.) + " Sek.");

		System.out.println("\n*** Aufgabe A3 Walk");
		startTime = System.currentTimeMillis();
		anzahl = fc.countDeepWalk("txt");
		endTime = System.currentTimeMillis();
		System.out.println("Es gibt " + anzahl + " Textdateien.");
		System.out.println("Der Vorgang dauerte: "
				+ ((endTime - startTime) / 1000.) + " Sek.");

		System.out.println("\n*** Aufgabe A3 walkFileTree");
		startTime = System.currentTimeMillis();
		anzahl = fc.countDeepWalkTree("txt");
		endTime = System.currentTimeMillis();
		System.out.println("Es gibt " + anzahl + " Textdateien.");
		System.out.println("Der Vorgang dauerte: "
				+ ((endTime - startTime) / 1000.) + " Sek.");
	}
}
