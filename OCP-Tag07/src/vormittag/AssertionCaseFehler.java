package vormittag;

enum Farbe {
	ROT, GELB, GRUEN, LILA
}

public class AssertionCaseFehler {

	public static void main(String[] args) {
		
		Ampel(Farbe.GELB); // kein Fehler
//		Ampel(Farbe.LILA); // Assertion Error
		System.out.println(berechne(6,0));
	}
	
	
	public static void Ampel(Farbe farbe) {
		switch (farbe) {
		case ROT:
			System.out.println("Halt!");
			break;
		case GELB:
			System.out.println("Bereit machen!");
			break;
		case GRUEN:
			System.out.println("Fahren!");
			break;
		default:
			assert false : "Farbe nicht erkannt";
		}
	}
	
	public static int berechne(int a, int b) {
		assert b!=0 : "Division durch 0";
		return a/ b;
	}
}
