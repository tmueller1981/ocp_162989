package vormittag;

import java.io.*;

public class FileUtils {

	public static void main(String[] args) {

		copyTextFile("source.txt", "copy.txt");
	}

	static void copyTextFile(String fromFile, String toFile) {

		try (Reader fr = new FileReader(new File(fromFile));
				Writer fw = new FileWriter(toFile)) {

			int ch;
			while ((ch = fr.read()) != -1) {
				fw.append((char) ch);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
