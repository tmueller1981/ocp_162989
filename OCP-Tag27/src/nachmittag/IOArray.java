package nachmittag;
import java.io.*;

public class IOArray {

	public static void main(String[] args) {

		int len = new java.util.Random().nextInt(100) + 1;
		int[] arr1 = createArray(len, -20, 20);

		System.out.println("*** Aufgabe 01");
		for (int i : arr1)
			System.out.print(i + " ");

		System.out.println("\n\n*** Aufgabe 02");
		saveArray(arr1, "array.txt");
		System.out.println("gespeichert");

		System.out.println("\n*** Aufgabe 03");
		int[] arr2 = loadArray("array.txt");
		for (int i : arr2)
			System.out.print(i + " ");

		System.out.println("\n*** Aufgabe 05");
		Console cons = System.console();

		String wahl = cons
				.readLine("M�chten Sie ein Array Erzeugen (1) oder laden(2) ?");

		switch (wahl) {
			case "1" :
				String size = cons
						.readLine("Bitte geben Sie die Gr��e des Arrays ein:");
				String fileName = cons.readLine(
						"Bitte geben Sie den Dateinamen mit Endung ein:");
				int[] userArr = createArray(Integer.parseInt(size), -20, 20);
				saveArray(userArr, fileName);
				System.out.println(
						"Array in Datei " + fileName + " gespeichert.");
				break;
			case "2" :
				String fileName2 = cons.readLine(
						"Bitte geben Sie den Dateinamen mit Endung ein:");
				int[] userArr2 = loadArray(fileName2);
				for (int i : userArr2)
					System.out.print(i + " ");
		}

	}

	static int[] createArray(int length, int min, int max) {

		int[] arr = new int[length];
		for (int i = 0; i < length; i++) {
			arr[i] = new java.util.Random().nextInt(max - min) + min;
		}

		return arr;
	}

	static void saveArray(int[] array, String fileName) {

		File file = new File(fileName);
		try (BufferedWriter bf = new BufferedWriter(new FileWriter(file));) {

			for (int i : array)
				bf.write(String.valueOf(i) + ", ");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static int[] loadArray(String fileName) {
		int[] arr = null;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

			String text = br.readLine();
			text = text.trim();
			String[] textArr = text.split(",");
			int len = textArr.length;
			arr = new int[len];

			for (int i = 0; i < textArr.length; i++) {
				arr[i] = Integer.valueOf(textArr[i].trim());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return arr;
	}
}
