package uebungen;


class Ueberschreiben {
	void test(int i) {}
	int test2(int i) { return 1; }
}

class Ueberladen {
	void test3(int i) {};
	int test4(int i) { return 1; }
}


class c1 extends Ueberschreiben {
	@Override
	void test(int i) {} // Parameter darf nicht geändert oder erweitert werden
	
	@Override
	int test2(int i) { return 1; } // Rückgabetyp und Parameeter darf nicht geändert werden
}

class c2 extends Ueberladen {
	void test3(String t) {}; // Parameter darf geändert und erweitert werden
	void test3(float f) {}
	String test4(double x) { return ""; } // Rückgabetyp und Parameter dürfen geändert werden	
}

public class UeberschreibenUeberladen {

}
