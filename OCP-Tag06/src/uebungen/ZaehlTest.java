package uebungen;

public class ZaehlTest {

	public static void main(String[] args) {
		
		/**
		 * Achtung das k bei der Referenz wird mitgezählt, daher ist das Ergebnis 6
		 */
		
		int k = 1;
		k += k + 4;
		System.out.println(k);
	}
}
