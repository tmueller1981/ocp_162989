package aufgaben;

enum Hunderasse {
	DACKEL(0.5), COLLIE(1.0), DOGGE(1.5);
	
	private double maxGroesse;
	
	private Hunderasse(double maxGroesse) {
		this.maxGroesse = maxGroesse;
	}
	
	public double getMaxGroesse() {
		return maxGroesse;
	}
	
	@Override
	public String toString() {
		String name = name().substring(1).toLowerCase();
		
		return name().charAt(0) + name + ", max. Größe: " + getMaxGroesse();
	}
}

public class HunderassenEnum {

	public static void main(String[] args) {
		
		for(Hunderasse hr : Hunderasse.values()) {
			System.out.println( hr );
		}
	}
}
