package vormittag;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class AufgabeTreeSet {

	static class Tier {
		String name;
		int alter;
		
		public Tier(String name, int alter) {
			this.name = name;
			this.alter = alter;
		}
		
		public int getAlter() {
			return this.alter;
		}
		
		@Override
		public String toString() {
			return name + " " + alter;
		}
		
		
	}
	
	public static void main(String[] args) {
		
		Comparator<Tier> name = (t1, t2) -> t1.name.compareTo(t2.name);
		Comparator<Tier> alter = Comparator.comparing(Tier::getAlter);
		Comparator<Tier> cmp = name.thenComparing(alter);
		TreeSet<Tier> ts = new TreeSet<>(cmp);
		ts.add(new Tier("Tom", 3));
		ts.add(new Tier("Jerry", 2));
		ts.add(new Tier("Tom", 1));
		ts.add(new Tier("Tom", 100));
		
		for (Tier tier : ts) {
			System.out.println(tier);
		}
		
		SortedSet<Tier> sub = ts.subSet(new Tier("Tom",100), true, new Tier("Tom",1), true);
		for (Tier tier : sub) {
			System.out.println(tier);
		}
	}

}
