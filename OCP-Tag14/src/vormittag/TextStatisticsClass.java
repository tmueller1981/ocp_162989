package vormittag;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

public class TextStatisticsClass {

	public static TextStatisticsClass of(String text) {
		return new TextStatisticsClass(text);
	}
	
	private String text;
	private TextStatisticsClass(String text) {
		this.text = text;
	}
	
	public Collection<Character> getUniqueChars() {
		
		Collection<Character> coll = new LinkedHashSet<>();
		
		for(int i = 0; i < text.length(); i++) {
			coll.add(text.charAt(i));
		}
		return coll;
	}
}
