package vormittag;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class FileTypes {

	private File root;
	public FileTypes(String pathName) {
		 root = new File(pathName);
		 
		 if(!root.isDirectory()) {
			 throw new IllegalArgumentException(pathName + "must be a directory!");
		 }
	}
	
	
	public Collection<String> getFileTypes() {
	
		Collection<String> extensions = new HashSet<>();
		
//		FileFilter filterFiles = path -> path.isFile();
		FileFilter filterFiles = File::isFile; // Methodenreferenz
		
		File[] files = root.listFiles(filterFiles);
		for (File file : files) {
			String ext = getExtension(file);
			extensions.add(ext);
		}
		
		return extensions;
	}
	
	private static String getExtension(File file) {
		
		String name = file.getName();
		int posOfPoint = name.lastIndexOf(".");
		
		if(posOfPoint <= 0 || posOfPoint==name.length()-1) {
			return "<no extension>";
		}
		
		
		return name.substring(posOfPoint + 1).toLowerCase();
	}
}
