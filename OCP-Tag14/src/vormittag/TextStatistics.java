package vormittag;

import java.util.Collection;

public class TextStatistics {

	public static void main(String[] args) {
		
		TextStatisticsClass stat = TextStatisticsClass.of("heute ist Montag!");
		
		Collection<Character> chars = stat.getUniqueChars();
		
		System.out.println("chars: " + chars);
	}
}
