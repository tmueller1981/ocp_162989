package vormittag;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Dienst {
	String name;

	public Dienst() {

	}

	public Dienst(String name) {
		this.name = name;
	}
}

class Defragmentierung extends Dienst {
	int zeitabstand;
	String laufwerk;

	public Defragmentierung(int zeitabstand, String laufwerk) {
		super(laufwerk);
		this.zeitabstand = zeitabstand;
		this.laufwerk = laufwerk;
	}

	@Override
	public String toString() {
		return "( Zeitabstand: " + zeitabstand + ", Laufwerk: " + laufwerk
				+ " )";
	}
}

class SpeicherManager extends Dienst implements Serializable {
	int size;
	transient Defragmentierung defrag;

	public SpeicherManager(int size, Defragmentierung defrag) {
		super(defrag.laufwerk);
		this.size = size;
		this.defrag = defrag;
	}

	@Override
	public String toString() {
		return "Manager. Size: " + size + ". Defrag-Dienst: " + defrag;
	}

	private void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		oos.writeUTF(defrag.laufwerk);
		oos.writeInt(defrag.zeitabstand);
	}

	private void readObject(ObjectInputStream ois)
			throws IOException, ClassNotFoundException {
		ois.defaultReadObject();
		String laufwerk = ois.readUTF();
		int size = ois.readInt();
		this.defrag = new Defragmentierung(size, laufwerk);

	}
}

public class AufgabeSerialisieren {

	public static void main(String[] args) {

		SpeicherManager sManager = new SpeicherManager(2000,
				new Defragmentierung(3000, "C:\\"));

		System.out.println("*** Aufgabe 01");
		System.out.println(sManager);

		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream("sManager.bin"))) {

			oos.writeObject(sManager);
			System.out.println("\n*** Aufgabe 02");
			System.out.println("In Datei geschrieben.");

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("\n*** Aufgabe 03");
		try (ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream("sManager.bin"))) {

			SpeicherManager sm2 = (SpeicherManager) ois.readObject();
			System.out.println(sm2);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
