package nachmittag;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Lebewesen {
	int alter;

	public Lebewesen() {

	}

	public Lebewesen(int alter) {
		this.alter = alter;
	}
}

class Mensch extends Lebewesen {
	String vorname, nachname;

	public Mensch(String vorname, String nachname) {
		super(0);
		this.vorname = vorname;
		this.nachname = nachname;
	}

	@Override
	public String toString() {
		return vorname + " " + nachname;
	}
}

class Hund extends Lebewesen implements Serializable {
	String name;
	transient Mensch besitzer;

	public Hund(int alter, String name, Mensch besitzer) {
		super(alter);
		this.name = name;
		this.besitzer = besitzer;
	}

	@Override
	public String toString() {
		return "Hund: " + name + ", Alter: " + alter + ". Besitzer: "
				+ besitzer;
	}

	private void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		oos.writeUTF(besitzer.vorname);
		oos.writeUTF(besitzer.nachname);
		oos.writeInt(super.alter);
	}

	private void readObject(ObjectInputStream ois)
			throws IOException, ClassNotFoundException {
		ois.defaultReadObject();
		String vorname = ois.readUTF();
		String nachname = ois.readUTF();
		besitzer = new Mensch(vorname, nachname);
		super.alter = ois.readInt();
	}
}

public class TestLebewesen {

	public static void main(String[] args) {

		Hund h = new Hund(2, "Max", new Mensch("Otto", "Meyer"));

		System.out.println("Aufgabe 01");
		System.out.println(h);

		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream("hund.txt"))) {

			oos.writeObject(h);
			System.out.println("\nAufgabe 02 - Datei geschrieben");
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream("hund.txt"))) {

			System.out.println("\nAufgabe 02 - Datei gelesen");
			Hund h1 = (Hund) ois.readObject();
			System.out.println(h1);

		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
}
